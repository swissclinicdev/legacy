<?php


namespace Swissclinic\Legacy\Test\Integration;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;
use Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection as VoteCollection;
use Magento\Review\Model\Review;
use PHPUnit\Framework\TestCase;
use Swissclinic\Legacy\Model\ImportReviews;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Framework\Module\Dir;

class ImportReviewsTest extends TestCase
{
    /**
     * @var ImportReviews
     */
    private $_importModel;

    /**
     * @var ReviewCollection
     */
    private $_reviewCollection;

    /**
     * @var VoteCollection
     */
    private $_voteCollection;

    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepo;

    /**
     * @magentoDataFixture Magento/Catalog/_files/product_simple.php
     */
    public function testGoodImport()
    {
        $this->_importModel = Bootstrap::getObjectManager()->get(ImportReviews::class);
        $this->_reviewCollection = Bootstrap::getObjectManager()->get(ReviewCollection::class);
        $this->_voteCollection = Bootstrap::getObjectManager()->get(VoteCollection::class);
        $this->_productRepo = Bootstrap::getObjectManager()->get(ProductRepositoryInterface::class);

        $moduleDir = Bootstrap::getObjectManager()->get(Dir::class);
        /* @var $moduleDir Dir */
        $moduleDirPath = $moduleDir->getDir('Swissclinic_Legacy');
        $this->_importModel->import($moduleDirPath . '/Test/_files/legacy_reviewimport.csv');

        $testProduct = $this->_productRepo->get('simple');
        $this->_reviewCollection->addEntityFilter('product',$testProduct->getId());

        // Should be only one review and one vote after this test
        $review = $this->_reviewCollection->getFirstItem();
        /* @var $review Review */
        $this->assertTrue(is_numeric($review->getId()));

        $vote = $this->_voteCollection->addFieldToFilter('review_id',$review->getId())->getFirstItem();
        // Compare vote value with value in test file
        $this->assertEquals(4, (int)$vote->getData('value'));
    }
}