<?php
namespace Swissclinic\Legacy\Test\Integration;

use Magento\Store\Model\StoreManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Swissclinic\Legacy\Model\ResourceModel\ImportUrls;

class ImportUrlsResourceTest extends TestCase
{
    /**
     * @var ImportUrls
     */
    private $_importResource;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @magentoDataFixture Magento/Store/_files/core_fixturestore.php
     */
    public function testGoodImportAndFailureOnDuplicate()
    {
        $data = ['some-old-path', 'some-new-path'];

        $this->_importResource = Bootstrap::getObjectManager()->get(ImportUrls::class);
        $this->_storeManager = Bootstrap::getObjectManager()->get(StoreManagerInterface::class);
        $store = $this->_storeManager->getStore('fixturestore');
        $this->_importResource->importRewrite($data);
        $this->assertEquals(ImportUrls::STATUS_SUCCESS, $this->_importResource->getLastImportStatus());

        $this->_importResource->importRewrite($data);
        $this->assertEquals(ImportUrls::STATUS_FAILURE_DUPLICATE, $this->_importResource->getLastImportStatus());
    }

    public function testInvalidRequestPathFormatImport()
    {
        $data = ['https://example.com/some-old-path', 'https://example.com/fixturestore/some-new-path'];

        $this->_importResource = Bootstrap::getObjectManager()->get(ImportUrls::class);
        $this->_importResource->importRewrite($data);
        $this->assertEquals(ImportUrls::STATUS_FAILURE_REQUEST_FORMAT, $this->_importResource->getLastImportStatus());
    }
}