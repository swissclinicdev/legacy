<?php


namespace Swissclinic\Test\Integration;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Module\Dir;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Customer\Api\CustomerRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Swissclinic\Legacy\Model\ImportCustomers;


class ImportCustomersTest extends TestCase
{
    /**
     * @var ImportCustomers
     */
    private $_importModel;

    /**
     * @var CustomerRepositoryInterface:
     */
    private $_customerRepo;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $_websiteRepo;

    public function testGoodImportAndFailOnDuplicate()
    {
        $this->_importModel = Bootstrap::getObjectManager()->get(ImportCustomers::class);

        $moduleDir = Bootstrap::getObjectManager()->get(Dir::class);
        /* @var $moduleDir Dir */
        $moduleDirPath = $moduleDir->getDir('Swissclinic_Legacy');
        $this->_importModel->import($moduleDirPath . '/Test/_files/legacy_customerimport.csv', ['base' => 'base']);
        $this->_websiteRepo = Bootstrap::getObjectManager()->get(WebsiteRepositoryInterface::class);
        $this->_customerRepo = Bootstrap::getObjectManager()->get(CustomerRepositoryInterface::class);
        try {
            // Get the customer and check values
            $website = $this->_websiteRepo->get('base');
            $importedCustomer = $this->_customerRepo->get('test@example.com', $website->getId());
            $this->assertTrue(is_numeric($importedCustomer->getId()));
            $this->assertEquals('John', $importedCustomer->getFirstname());
            $this->assertEquals('Doe', $importedCustomer->getLastname());
            $this->assertEquals('legacy_username_value', $importedCustomer->getCustomAttribute('legacy_username')->getValue());

            // Check address values
            $addresses = $importedCustomer->getAddresses();
            foreach($addresses as $address) {
                $street = $address->getStreet();
                $this->assertEquals('testgatan1', $street[0]);
                $this->assertEquals('c/o Husman', $street[1]);
                $this->assertEquals('11221', $address->getPostcode());
                $this->assertEquals('Stockholm', $address->getCity());
            }
        } catch (NoSuchEntityException $e) {
            $this->fail('Imported customer not found by customer repository');
        }

        // Import same customer again; this should fail because the imported customer already exists since the first import.
        $this->_importModel->import($moduleDirPath . '/Test/_files/legacy_customerimport.csv', ['base' => 'base']);
        $failedImportModel = Bootstrap::getObjectManager()->get(\Swissclinic\Legacy\Model\FailedCustomerImport::class);
        /* @var $failedImportModel \Swissclinic\Legacy\Model\FailedorderImport */
        $failedImportInstance = $failedImportModel->getCollection()->getFirstItem();
        $this->assertContains('test@example.com', $failedImportInstance->getData('log_message'));
    }
}