<?php
namespace Swissclinic\Legacy\Test\Integration;

use Magento\SalesRule\Model\Data\Condition;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Module\Dir;
use Magento\SalesRule\Api\CouponRepositoryInterface;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use Magento\SalesRule\Model\Rule;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Swissclinic\Legacy\Model\ImportDiscountCodes;
use Swissclinic\Legacy\Model\ImportCartRules;

class ImportCartRulesAndDiscountCodesTest extends TestCase
{
    /**
     * @var ImportDiscountCodes
     */
    private $_couponImportModel;

    /**
     * @var ImportCartRules
     */
    private $_ruleImportModel;

    /**
     * @var CouponRepositoryInterface
     */
    private $_couponRepo;

    /**
     * @var RuleRepositoryInterface
     */
    private $_salesRuleRepo;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * Test import of both cart rules and discounts codes, since the latter depends on the former.
     *
     * @magentoDataFixture Magento/SalesRule/_files/rules_autogeneration.php
     */
    public function testGoodImport()
    {
        $this->_couponImportModel = Bootstrap::getObjectManager()->get(ImportDiscountCodes::class);
        $this->_ruleImportModel = Bootstrap::getObjectManager()->get(ImportCartRules::class);
        $this->_couponRepo = Bootstrap::getObjectManager()->get(CouponRepositoryInterface::class);
        $this->_salesRuleRepo = Bootstrap::getObjectManager()->get(RuleRepositoryInterface::class);
        $this->_searchCriteriaBuilder = Bootstrap::getObjectManager()->get(SearchCriteriaBuilder::class);

        $moduleDir = Bootstrap::getObjectManager()->get(Dir::class);
        /* @var $moduleDir Dir */
        $moduleDirPath = $moduleDir->getDir('Swissclinic_Legacy');

        // Import test file and assert correct values for the imported rule
        $this->_ruleImportModel->import($moduleDirPath . '/Test/_files/legacy_cartruleimport.csv');
        $ruleSearchCriteria = $this->_searchCriteriaBuilder->addFilter('name', 'testrule')->create();
        $rules = $this->_salesRuleRepo->getList($ruleSearchCriteria)->getItems();
        $this->assertEquals(1, count($rules));  // Import should result in 1 rule
        $rule = array_shift($rules);
        /* @var $rule Rule */
        $this->assertEquals('testrule', $rule->getName());

        $conditions = $rule->getCondition()->getConditions();
        $this->assertEquals(1, count($conditions)); // Import should result in rule with 1 condition
        $condition = array_shift($conditions);
        /* @var $condition Condition */
        // Assertions for the salesrule condition that the test import file should create
        $this->assertEquals('Magento\SalesRule\Model\Rule\Condition\Address', $condition->getConditionType());
        $this->assertEquals('total_qty', $condition->getAttributeName());
        $this->assertEquals('>', $condition->getOperator());
        $this->assertEquals(2, $condition->getValue());

        $this->_couponImportModel->import($moduleDirPath . '/Test/_files/legacy_discountcodeimport.csv');
        $couponSearchCriteria = $this->_searchCriteriaBuilder->addFilter('code', 'testdiscount')->create();
        $coupons = array_values($this->_couponRepo->getList($couponSearchCriteria)->getItems());

        // Import test file and assert correct values for the coupon
        $this->assertEquals(1, count($coupons)); // Import should result in 1 coupon
        $coupon = array_shift($coupons);
        $this->assertEquals('testdiscount', $coupon->getCode());
    }
}