<?php

namespace Swissclinic\Test\Integration;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Module\Dir;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Test\Block\Adminhtml\Order\Create\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Swissclinic\Legacy\Model\ImportOrders;


class ImportCustomersTest extends TestCase
{
    /**
     * @var ImportOrders
     */
    private $_importModel;

    /**
     * @var OrderRepositoryInterface:
     */
    private $_orderRepo;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @magentoDataFixture Magento/Catalog/_files/product_simple.php
     */
    public function testGoodImport()
    {
        $this->_importModel = Bootstrap::getObjectManager()->get(ImportOrders::class);
        $this->_storeManager = Bootstrap::getObjectManager()->get(StoreManagerInterface::class);
        $storeCurrency = $this->_storeManager->getStore('default')->getCurrentCurrency()->getCode();

        $moduleDir = Bootstrap::getObjectManager()->get(Dir::class);
        /* @var $moduleDir Dir */
        $moduleDirPath = $moduleDir->getDir('Swissclinic_Legacy');
        $this->_importModel->import($moduleDirPath . '/Test/_files/legacy_orderimport.csv', ['base' => 'base']);
        $this->_orderRepo = Bootstrap::getObjectManager()->get(OrderRepositoryInterface::class);
        $this->_searchCriteriaBuilder = Bootstrap::getObjectManager()->get(SearchCriteriaBuilder::class);
        try {
            $this->_searchCriteriaBuilder->addFilter('customer_email', 'test@test.com');
            $criteria = $this->_searchCriteriaBuilder->create();
            $orders = $this->_orderRepo->getList($criteria);
            foreach($orders->getItems() as $order) {
                // Compare some values with the test csv file to assert import success
                $this->assertTrue(is_numeric($order->getId()));
                $this->assertEquals($storeCurrency, $order->getOrderCurrencyCode());
                $orderItems = $order->getItems();
                foreach($orderItems as $item) {
                    $this->assertEquals('simple', $item->getSku());
                }
                $this->assertEquals('WC2N 5DU', $order->getBillingAddress()->getPostcode());
            }
        } catch (NoSuchEntityException $e) {
            $this->fail('Imported order not found');
        }
    }
}