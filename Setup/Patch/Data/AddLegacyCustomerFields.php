<?php declare(strict_types=1);

namespace Swissclinic\Legacy\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

/**
 * Class AddLegacyFields
 * @package Swissclinic\Legacy\Setup\Patch
 */
class AddLegacyCustomerFields implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    CONST ATTRIBUTE_SET_ID = 'attribute_set_id';
    CONST ATTRIBUTE_GROUP_ID = 'attribute_group_id';

    /**
     * AddLegacyFields constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
        /*
         * Add legacy customer_attribute field.
         */
        $customerSetup->addAttribute(
            Customer::ENTITY,
            'legacy_accept_emails_offer',
            [
                'type' => 'varchar',
                'label' => 'Accepts Email offer',
                'input' => 'text',
                'required' => false,
                'sort_order' => 100,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false,
            ]
        );

        $customerSetup->addAttribute(
            Customer::ENTITY,
            'legacy_phone_offers',
            [
                'type' => 'varchar',
                'label' => 'Accepts Phone offers',
                'input' => 'text',
                'required' => false,
                'sort_order' => 110,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false
            ]
        );

        $attributeList = [
            'legacy_phone_offers',
            'legacy_accept_emails_offer'
        ];

        $this->assignAttributes($attributeList,Customer::ENTITY);

        /*
         * Add legacy "customer_address" attribute field.
         */
        $customerSetup->addAttribute(
            'customer_address',
            'legacy_address2',
            [
                'type' => 'varchar',
                'label' => 'Legacy second address information.',
                'input' => 'text',
                'required' => false,
                'sort_order' => 110,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false
            ]
        );

        $attributeList = ['legacy_address2'];
        $this->assignAttributes($attributeList,'customer_address');

    }

    /**
     * @param $attributeList
     * @param $entityType
     */
    protected function assignAttributes($attributeList, $entityType){
        $customerSetup = $this->customerSetupFactory->create([
            'setup' => $this->moduleDataSetup]
        );
        $customerEntity = $customerSetup->getEavConfig()->getEntityType($entityType);

        $attributeSet = $this->attributeSetFactory->create();
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        foreach ($attributeList as $attributeCode) {
            $attribute = $customerSetup->getEavConfig()->getAttribute(
                $entityType,
                $attributeCode
            );
            $attribute->addData(
                [
                    self::ATTRIBUTE_SET_ID => $attributeSetId,
                    self::ATTRIBUTE_GROUP_ID => $attributeGroupId,
                    'used_in_forms' => [
                        'adminhtml_customer'
                    ]
                ]
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}