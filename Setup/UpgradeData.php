<?php

namespace Swissclinic\Legacy\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\DB\Ddl\Table;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * UpgradeSchema constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }
    protected function _removeAttributes($_attributeCodes, $strEntityType){
        $customerSetup =  $this->customerSetupFactory->create();

        foreach ($_attributeCodes as $key => $value){
            $customerSetup->removeAttribute($strEntityType,$key);
        }

    }

    protected function _addAttributes($_attributeCodes, $strEntityType){
        $customerSetup =  $this->customerSetupFactory->create();

        foreach ($_attributeCodes as $attribCode => $params){
            $customerSetup->addAttribute($strEntityType, $attribCode, $params);
        }

    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // let's add the attributes to the group and set.

        if(version_compare($context->getVersion(), '0.0.9', '<=')){

            $_legacyCustomerFields = $this->_getLegacyCustomerAttributes();
            $_legacyCustomerAddressFields = $this->_getLegacyCustomerAddressAttributes();

            //Remove previous attributes
            $this->_removeAttributes($_legacyCustomerFields,\Magento\Customer\Model\Customer::ENTITY);
            $this->_removeAttributes($_legacyCustomerAddressFields,'customer_address');

            // Add attributes
            $this->_addAttributes($_legacyCustomerFields,\Magento\Customer\Model\Customer::ENTITY);
            $this->_addAttributes($_legacyCustomerAddressFields,'customer_address');

            // Assign customer attributes to group set and default attribute group.
            $this->_addAttributeGroupAndSet($_legacyCustomerFields,\Magento\Customer\Model\Customer::ENTITY);

            // Assign customer_address attributes to group set and default attribute group.
            $this->_addAttributeGroupAndSet($_legacyCustomerAddressFields,'customer_address');
        }

        $setup->endSetup();
    }

    /**
     * @param $_attributeCodes
     * @param $strEntityType
     */
    protected function _addAttributeGroupAndSet($_attributeCodes, $strEntityType){


        $customerSetup =  $this->customerSetupFactory->create();
        $customerEntity = $customerSetup->getEavConfig()->getEntityType($strEntityType);

        // get the set id
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        // get the group id
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);


        foreach ($_attributeCodes as $_attributeCode => $_params){

            $attribute = $customerSetup->getEavConfig()
                ->getAttribute($strEntityType, $_attributeCode)
                ->addData(
                    [
                        'attribute_set_id' => $attributeSetId,
                        'attribute_group_id' => $attributeGroupId,
                        'used_in_forms'=> ['adminhtml_customer']
                    ]
                );
            $attribute->save();
        }
    }

    /**
     * @return array
     */
    public function _getLegacyCustomerAddressAttributes(){
        $legacyAttributes= [

            'legacy_adress2_extra' => [
                'type' => 'varchar',
                'label' => 'legacy_adress2_extra',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_adress_extra' => [
                'type' => 'varchar',
                'label' => 'legacy_adress_extra',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],

            'legacy_username' => [
                'type' => 'varchar',
                'label' => 'legacy_username',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_efternamn2' => [
                'type' => 'varchar',
                'label' => 'legacy_efternamn2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_email2' => [
                'type' => 'varchar',
                'label' => 'legacy_email2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_name2' => [
                'type' => 'varchar',
                'label' => 'legacy_name2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_kundid' => [
                'type' => 'varchar',
                'label' => 'legacy_kundid',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_kundkategori_id ' => [
                'type' => 'varchar',
                'label' => 'legacy_kundkategori_id',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_country_id2' => [
                'type' => 'varchar',
                'label' => 'legacy_country_id2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_city2' => [
                'type' => 'varchar',
                'label' => 'legacy_city2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_postcode2' => [
                'type' => 'varchar',
                'label' => 'legacy_postcode2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_stat' => [
                'type' => 'varchar',
                'label' => 'legacy_stat',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'legacy_stat2' => [
                'type' => 'varchar',
                'label' => 'legacy_stat2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'mobile_phone' => [
                'type' => 'varchar',
                'label' => 'mobile_phone',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'telephone2' => [
                'type' => 'varchar',
                'label' => 'telephone2',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
            'telephone3' => [
                'type' => 'varchar',
                'label' => 'telephone3',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ],
        ];
        return $legacyAttributes;

    }
    /**
     * @return array
     */
    public function _getLegacyCustomerAttributes(){


        $legacyAttributes= [
            'legacy_username'=>[
                'type' => 'varchar',
                'label' => 'legacy_username',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'sales_consent_contact'=>[
                'type' => 'varchar',
                'label' => 'sales_consent_contact',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'sales_consent_mobile'=>[
                'type' => 'varchar',
                'label' => 'sales_consent_mobile',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'internal_comments'=>[
                'type' => 'varchar',
                'label' => 'internal_comments',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_konto'=>[
                'type' => 'varchar',
                'label' => 'legacy_konto',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_kundid'=>[
                'type' => 'varchar',
                'label' => 'legacy_kundid',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_server'=>[
                'type' => 'varchar',
                'label' => 'legacy_server',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_tiderbjudepost'=>[
                'type' => 'varchar',
                'label' => 'legacy_tiderbjudepost',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_tid_aktivitet'=>[
                'type' => 'varchar',
                'label' => 'legacy_tid_aktivitet',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_tid_registrerad'=>[
                'type' => 'varchar',
                'label' => 'legacy_tid_registrerad',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_tid_uppdaterad'=>[
                'type' => 'varchar',
                'label' => 'legacy_tid_uppdaterad',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,

            ],
            'legacy_country'=>[
                'type' => 'varchar',
                'label' => 'legacy_country',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ]
            ,
            'sum'=>[
                'type' => 'varchar',
                'label' => 'sum',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'sort_order' => 100,
                'position' => 100,
                'system' => 0,
            ]
        ];

        return $legacyAttributes;

    }
}