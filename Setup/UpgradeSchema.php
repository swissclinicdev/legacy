<?php


namespace Swissclinic\Legacy\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;


class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * UpgradeSchema constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        if(version_compare($context->getVersion(),'0.0.7','<=')){
            $this->_createCustomerTable($setup);
        }

        if(version_compare($context->getVersion(), '1.0.0', '<=')){
            $this->_createLegacyDiscountTable($setup);
            $this->_createFailedDiscountTable($setup);
            // Create a table that will be holding extra information about the discount codes.

        }

        $setup->endSetup();
    }

    protected function _createCustomerTable($setup){
        /**
         * Create table 'swissclinic_legacy_failed_customer_import'
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('swissclinic_legacy_failed_customer_import'))
            ->addColumn(
                'import_transaction_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Import transaction ID'
            )

            ->addColumn(
                'log_message',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1000,
                ['nullable' => true],
                'log message captured'
            )
            ->setComment('This table contains failed customer imports that needs to be reviewed by an agent.');

        $setup->getConnection()->createTable($table);
    }

    protected function _createLegacyDiscountTable($setup){

        /**
         * Create table 'swissclinic_legacy_sale_rule'
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('swissclinic_legacy_sale_rule'))
            ->addColumn(
                'rule_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Import transaction ID'
            )

            ->addColumn(
                'rabatt_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'legacy_rabatt_id'
            )
            ->addColumn(
                'rabattkod',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                30,
                ['nullable' => true],
                'legacy_rabatt_id'
            )
            ->addColumn(
                'tid',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => true],
                'This one is used not available via Magento SalesRule Api Data RuleInterface - however it should be saved in a separate table for future use + record keeping. legacy_tid'
            )
            ->addColumn(
                'rabatt_foralder',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => true],
                'will always be 0 for parents, single use codes will have the rule id'
            )
            ->addColumn(
                'tid_uppdaterad',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => true],
                ' This one is used not available via Magento SalesRule Api Data RuleInterface - however it should be saved in a separate table for future use + record keeping. legacy_tid_uppdaterad'
            )
            ->setComment('Contains information from the legacy discount codes. To be kept for booking purposes.');

        $setup->getConnection()->createTable($table);
    }

    protected function _createFailedDiscountTable($setup){
        /**
         * Create table 'swissclinic_legacy_failed_customer_import'
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('swissclinic_legacy_failed_discount_import'))
            ->addColumn(
                'import_transaction_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Import transaction ID'
            )

            ->addColumn(
                'log_message',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                1000,
                ['nullable' => true],
                'log message captured'
            )
            ->setComment('This table contains failed discount imports that needs to be reviewed by an agent.');

        $setup->getConnection()->createTable($table);
    }

}
