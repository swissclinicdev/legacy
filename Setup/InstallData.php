<?php

namespace Swissclinic\Legacy\Setup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;


    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * UpgradeSchema constructor.
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
    ){
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;

    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
                            ModuleDataSetupInterface $setup,
                            ModuleContextInterface $context
                           )
    {
        $setup->startSetup();
        $_legacyCustomerFields = $this->_getLegacyCustomerAttributes();
        $_legacyCustomerAddressFields = $this->_getLegacyCustomerAddressAttributes();

        if (version_compare($context->getVersion(), '0.0.2', '<=')) {

            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            /*
             * Lets begin by adding our legacy attributes to the customer entity.
             */

            $this->_addLegacyAttributes($customerSetup, $_legacyCustomerFields, \Magento\Customer\Model\Customer::ENTITY);
            /**
             * Time to create the custom address attributes.
             */
            $this->_addLegacyAttributes($customerSetup, $_legacyCustomerAddressFields, 'customer_address');
        }

        $setup->endSetup();
    }


    /**
     * @return array
     */
    protected function _getLegacyCustomerAddressAttributes(){
        $legacyAttributes= [

            'legacy_adress2_extra' => [
                'label' => 'legacy_adress2_extra',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_adress_extra' => [
                'label' => 'legacy_adress_extra',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],

            'legacy_username' => [
                'label' => 'legacy_username',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_efternamn2' => [
                'label' => 'legacy_efternamn2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_email2' => [
                'label' => 'legacy_email2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_name2' => [
                'label' => 'legacy_name2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_kundid' => [
                'label' => 'legacy_kundid',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_kundkategori_id ' => [
                'label' => 'legacy_kundkategori_id',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_country_id2' => [
                'label' => 'legacy_country_id2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_city2' => [
                'label' => 'legacy_city2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_postcode2' => [
                'label' => 'legacy_postcode2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_stat' => [
                'label' => ' legacy_stat',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'legacy_stat2' => [
                'label' => 'legacy_stat2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'mobile_phone' => [
                'label' => 'mobile_phone',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'telephone2' => [
                'label' => 'telephone2',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
            'telephone3' => [
                'label' => 'telephone3',
                'type' => 'varchar',
                'input' => 'text',
                'position' => 999,
                'visible' => true,
                'required' => false,
            ],
        ];
        return $legacyAttributes;

    }

    /**
     * @return array
     */
    protected function _getLegacyCustomerAttributes(){


        $legacyAttributes= [
            'legacy_username'=>[
                'label' => 'legacy_username',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'sales_consent_contact'=>[
                'label' => 'sales_consent_contact',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'sales_consent_mobile'=>[
                'label' => 'sales_consent_mobile',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'internal_comments'=>[
                'label' => 'internal_comments',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_konto'=>[
                'label' => 'legacy_konto',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_kundid'=>[
                'label' => 'legacy_kundid',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_server'=>[
                'label' => 'legacy_server',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_tiderbjudepost'=>[
                'label' => 'legacy_tiderbjudepost',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_tid_aktivitet'=>[
                'label' => 'legacy_tid_aktivitet',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_tid_registrerad'=>[
                'label' => 'legacy_tid_registrerad',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_tid_uppdaterad'=>[
                'label' => 'legacy_tid_uppdaterad',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false

            ],
            'legacy_country'=>[
                'label' => 'legacy_country',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false
            ]
            ,
            'sum'=>[
                'label' => 'sum',
                'input' => 'text',
                'system' => false,
                'visible' => true,
                'sort_order' => 999,
                'required' => false
            ]
        ];

        return $legacyAttributes;

    }

    /**
     * @param $customerSetup
     * @param $legacyAttributes
     * @param $entity
     */
    protected function _addLegacyAttributes($customerSetup, $legacyAttributes, $entity){

        foreach ($legacyAttributes as $attributeCode => $attributeParams) {
            $customerSetup->addAttribute($entity, $attributeCode, $attributeParams);
        }
    }
}