<?php

namespace Swissclinic\Legacy\Console\Command;

use Magento\Store\Model\StoreManagerInterface;
use Swissclinic\Legacy\Model\LegacyLoggerFactory;
use Swissclinic\Legacy\Model\ResourceModel\ImportUrls as ImportResource;
use Swissclinic\Legacy\Model\ResourceModel\ImportUrlsFactory as ImportResourceFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\File\CsvFactory;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command as Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ImportUrls extends Command {

    const LOG_FILENAME = 'legacy_url_import_error.log';

    /**
     * @var LegacyLoggerFactory
     */
    private $_loggerFactory;

    /**
     * @var Logger
     */
    private $_logger;

    /**
     * @var State
     */
    private $_appState;
    /**
     * @var CsvFactory
     */
    private $_csvFactory;

    /**
     * @var \Swissclinic\Legacy\Model\ResourceModel\ImportUrls
     */
    private $_importResource;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var int
     */
    private $_successes = 0;

    private $_importStatusMessages = [
        ImportResource::STATUS_FAILURE_UNKNOWN => 'Unknown error',
        ImportResource::STATUS_FAILURE_DUPLICATE => 'Rewrite already exists',
        ImportResource::STATUS_FAILURE_REQUEST_FORMAT => 'Invalid format of request path',
    ];

    const FILENAME_ARGUMENT = 'filename';
    const STORE_CODE_ARGUMENT = 'storecode';

    public function __construct(
        LegacyLoggerFactory $loggerFactory,
        State $appState,
        CsvFactory $csvFactory,
        ImportResourceFactory $resourceFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->_loggerFactory = $loggerFactory;
        $this->_appState = $appState;
        $this->_csvFactory = $csvFactory;
        $this->_importResource = $resourceFactory->create();
        $this->_storeManager = $storeManager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this
            ->setName('swissclinic:legacy:import:urls')
            ->setDescription('Imports url redirects from a csv file')
            ->setDefinition([
                new InputArgument(
                    self::FILENAME_ARGUMENT,
                    InputArgument::REQUIRED,
                    'Filename'
                ),
                new InputArgument(
                    self::STORE_CODE_ARGUMENT,
                    InputArgument::REQUIRED,
                    'Store code'
                )]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->_appState->setAreaCode(Area::AREA_ADMINHTML);
        $filename = $input->getArgument(self::FILENAME_ARGUMENT);
        $basename = pathinfo($filename, PATHINFO_BASENAME);
        if(!file_exists($filename)) {
            $output->writeln("<error>Sorry, cannot open the file $filename!</error>");
            exit();
        }

        $storeCode = $input->getArgument(self::STORE_CODE_ARGUMENT);
        $store = $this->_storeManager->getStore($storeCode);
        if(!$store->getId()) {
            $output->writeln("<error>Store with identifier \"$storeCode\" does not exist!</error>");
            exit();
        }

        $csvHandler = $this->_csvFactory->create();
        /* @var $csvHandler \Magento\Framework\File\Csv */
        $fileData = $csvHandler->getData($filename);
        $importCount = count($fileData) -1; // all rows except header
        $this->_logger = $this->_loggerFactory
            ->setLogFileName(self::LOG_FILENAME)
            ->create()
        ;

        $output->writeln("<info>Starting import of URLs:</info>");
        $headerRow = true;
        foreach($fileData as $fileDataRow) {
            if($headerRow) {
                $headerRow = false;
                continue;
            }
            $result = $this->_importResource->importRewrite($fileDataRow, $store, $basename);
            $this->_successes += $result;
            if($result < 1) {
                $lastImportStatus = $this->_importResource->getLastImportStatus();
                $this->_logger->error($this->_importStatusMessages[$lastImportStatus], $fileDataRow); // Log error message with offending data row
            }
        }

        $allSuccessful = ($this->_successes == $importCount); // comparing number of successes with total rows to import
        if($allSuccessful === true){
            $output->writeln("<info>The import was completed without critical errors.</info>");
        } else {
            $output->writeln(sprintf('<error>Import process was completed with errors. Check the log file %s</error>', self::LOG_FILENAME));
        }
    }
}