<?php

namespace Swissclinic\Legacy\Console\Command;

use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;
use Magento\Store\Model\StoreManager;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ImportOrders extends Command{

    protected $_objManager;
    protected $_importOrdersFactory;
    const FILENAME_ARGUMENT = 'filename';


    /**
     * ImportOrders constructor.
     * @param State $appState
     * @param \Swissclinic\Legacy\Model\ImportOrdersFactory $importOrdersFactory
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        \Swissclinic\Legacy\Model\ImportOrdersFactory $importOrdersFactory
    )
    {
        $this->_importOrdersFactory = $importOrdersFactory;
        $this->appState = $appState;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('swissclinic:legacy:import:orders');
        $this->setDescription('Imports orders from the legacy system')
        ->setDefinition([
        new InputArgument(
            self::FILENAME_ARGUMENT,
            InputArgument::OPTIONAL,
            'Filename'
        )]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)

    {
        $this->appState->setAreaCode(Area::AREA_FRONTEND);
        $_filename = $input->getArgument(self::FILENAME_ARGUMENT);
        if(!file_exists($_filename)){
            $output->writeln("<error>Sorry, cannot open the file $_filename!</error>");
            exit();
        }

        $_importModel = $this->_importOrdersFactory->create();
        $output->writeln("");
        $output->writeln("<info>Starting import of orders:</info>");
        $result = $_importModel->import($_filename);

        if($result && isset($result['successfull']) && $result['successfull']>0 && ($successfullOrder = $result['successfull'])){
            $output->writeln("<info>$successfullOrder orders were imported successfully:</info>");
        }
        if($result && isset($result['errors']) && $result['errors']>0 && ($failedOrder = $result['errors'])){
            $output->writeln("<error>Sorry, $failedOrder orders were not imported, you can find more about the failed orders in </error>");
        }

    }


}