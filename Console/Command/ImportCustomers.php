<?php

namespace Swissclinic\Legacy\Console\Command;

use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\App\State as AppState;
use Magento\Framework\App\Area;
use Magento\Store\Model\StoreManager;
use Swissclinic\Legacy\Model\ImportCustomersFactory;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Takes care of importing the customers.
 */

class ImportCustomers extends Command{

    const FILENAME_ARGUMENT = 'filename';
    /**
     * @var ImportCustomersFactory
     */
    protected $_importCustomersFactory;

    /**
     * @var Magento\Framework\App\State
     */
    protected $appState;

    /**
     * ImportCustomers constructor.
     * @param AppState $appState
     * @param ImportCustomersFactory $importCustomersFactory
     */
    public function __construct(
        AppState $appState,
        ImportCustomersFactory $importCustomersFactory)
    {
        $this->_importCustomersFactory = $importCustomersFactory;
        $this->appState = $appState;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('swissclinic:legacy:import:customers');
        $this->setDescription('Imports customers from the legacy system')
        ->setDefinition([
            new InputArgument(
                self::FILENAME_ARGUMENT,
                InputArgument::OPTIONAL,
                'Filename'
            )
        ]);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_FRONTEND);
        $_filename = $input->getArgument(self::FILENAME_ARGUMENT);
        if(!file_exists($_filename)){
            $output->writeln("<error>Sorry, cannot open the file $_filename!</error>");
            exit();
        }

        $_importModel = $this->_importCustomersFactory->create();
        $output->writeln("");
        $startTime = microtime(true);
        $output->writeln("<info>Starting import of customers:</info>");
        $_importModel->import($_filename);
        $resultTime = microtime(true) - $startTime;
        $output->writeln(
            "<info>Import completed successfully in: ".gmdate('H:i:s', $resultTime)."</info>"
        );
    }
}