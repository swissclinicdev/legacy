<?php

namespace Swissclinic\Legacy\Console\Command;

use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;
use Magento\Store\Model\StoreManager;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ImportReviews extends Command{

    protected $_objManager;
    protected $_importReviewsFactory;
    const FILENAME_ARGUMENT = 'filename';


    public function __construct(
        \Magento\Framework\App\State $appState,
        \Swissclinic\Legacy\Model\ImportReviewsFactory $importReviewsFactory
    )
    {
        $this->_importReviewsFactory = $importReviewsFactory;
        $this->appState = $appState;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('swissclinic:legacy:import:reviews');
        $this->setDescription('Imports reviews from the legacy system')
            ->setDefinition([
                new InputArgument(
                    self::FILENAME_ARGUMENT,
                    InputArgument::OPTIONAL,
                    'Filename'
                )]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_FRONTEND);
        $_filename = $input->getArgument(self::FILENAME_ARGUMENT);
        if(!file_exists($_filename)){
            $output->writeln("<error>Sorry, cannot open the file $_filename!</error>");
            exit();
        }

        $_importModel = $this->_importReviewsFactory->create();
        $output->writeln("");
        $output->writeln("<info>Starting import of reviews:</info>");
        if(($_result = $_importModel->import($_filename)) ){
            $output->writeln("<info>Let's congratulate this day, the import was completed without critical errors:</info>");
        }
        else {
            $output->writeln("<error>Sorry the import process was completed but some errors were found check the log file error_review_import.log:</error>");
        }


    }


}