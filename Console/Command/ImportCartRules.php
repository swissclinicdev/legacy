<?php

namespace Swissclinic\Legacy\Console\Command;

use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;
use Magento\Store\Model\StoreManager;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCartRules extends Command{

    protected $_objManager;
    protected $_importCartRulesFactory;
    const FILENAME_ARGUMENT = 'filename';


    public function __construct(
        \Magento\Framework\App\State $appState,
        \Swissclinic\Legacy\Model\ImportCartRulesFactory $importproductsFactory
    )
    {
        $this->_importCartRulesFactory = $importproductsFactory;
        $this->appState = $appState;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('swissclinic:legacy:import:cartrules');
        $this->setDescription('Imports cart rules from the legacy system')
            ->setDefinition([
                new InputArgument(
                    self::FILENAME_ARGUMENT,
                    InputArgument::OPTIONAL,
                    'Filename'
                )]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_FRONTEND);
        $_filename = $input->getArgument(self::FILENAME_ARGUMENT);
        if(!file_exists($_filename)){
            $output->writeln("<error>Sorry, cannot open the file $_filename!</error>");
            exit();
        }

        $_importModel = $this->_importCartRulesFactory->create();
        $output->writeln("");
        $output->writeln("<info>Starting import of cart rules:</info>");
        $_importModel->import($_filename);
        $output->writeln("<info>Import completed successfully:</info>");
    }


}