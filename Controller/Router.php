<?php
namespace Swissclinic\Legacy\Controller;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Swissclinic\Legacy\Controller\Redirect\Index;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\Action\Redirect;

/**
 * This Router matches requests to legacy base URLs and redirects to corresponding store
 * @package Swissclinic\Legacy\Controller
 */
class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var ResponseInterface
     */
    private $_response;

    /**
     * @var UrlInterface
     */
    private $_url;

    /**
     * @var ActionFactory
     */
    private $_actionFactory;

    /**
     * @var array
     */
    private $_indexedStores = [];

    /**
     * @var string
     */
    private $_requestHost;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        ResponseInterface $response,
        UrlInterface $url,
        ActionFactory $actionFactory
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_response = $response;
        $this->_url = $url;
        $this->_actionFactory = $actionFactory;

        // Index stores by legacy URL for easier access
        foreach($this->_storeManager->getStores() as $store) {
            $storeLegacyUrl = $this->_scopeConfig->getValue('web/secure/swissclinic_legacy_base_url', 'store', $store->getId());
            if($storeLegacyUrl) {
                $storeLegacyHost = parse_url($storeLegacyUrl, PHP_URL_HOST);
                $this->_indexedStores[$storeLegacyHost] = $store;
            }
        }
    }

    /**
     * Match against legacy url
     *
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        /* @var $request Http */
        $this->_requestHost = $request->getHttpHost();

        // Look for store with matching legacy base URL
        if(!isset($this->_indexedStores[$this->_requestHost])) {
            return null;
        }

        $store = $this->_indexedStores[$this->_requestHost];
        $this->_storeManager->setCurrentStore($store->getId());
        $pathinfo = ltrim($request->getOriginalPathInfo(), '/');
        $targetUrl = $this->_url->getDirectUrl($pathinfo);

        $this->_response->setRedirect($targetUrl, 301);
        return $this->_actionFactory->create(Redirect::class);
    }
}