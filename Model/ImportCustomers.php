<?php
namespace Swissclinic\Legacy\Model;

use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Swissclinic\Legacy\Model\FailedCustomerImportFactory;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\WebsiteFactory;
use Swissclinic\Legacy\Setup\UpgradeData;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Store\Model\StoreManagerInterface;

class ImportCustomers {

    protected $columnNames;
    protected $_logger;
    protected $_excludedColumns;
    protected $_customerObjectAttributes;
    protected $_customerAddressObjectAttributes;
    protected $_landList;
    protected $_websiteId;
    protected $_websiteCollection;
    protected $_customerDataFactory;
    protected $_customerRepositoryFactory;
    protected $_customerAddressDataFactory;
    protected $_failedCustomerImportFactory;
    protected $_websiteFactory;
    protected $_customCustomerAttributes;
    protected $_customCustomerAddressAttributes;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var array
     */
    protected $_addressRepository;
    /**
     * @var Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerFactory;
    /**
     * @var Swissclinic\Legacy\Model\FailedCustomerImportFactory
     */
    protected $_failedTransactions = [];
    /**
     * @var array
     */
    protected $mappedMagentoAttributeCodes;

    /**
     * @var array
     */
    protected $genderMapping;

    const DEFAULT_COUNTRY_ID_CODE = 'SE';
    const DEFAULT_WEBSITE_CODE ='sweden';

    /**
     * ImportCustomers constructor.
     * @param CustomerInterfaceFactory $customerDataInterfaceFactory
     * @param CustomerRepositoryInterface $customerRepositoryInterfaceFactory
     * @param AddressInterfaceFactory $customerAddressDataFactory
     * @param \Swissclinic\Legacy\Model\FailedCustomerImportFactory $failedCustomerImportFactory
     * @param LoggerInterface $logger
     * @param WebsiteFactory $websiteFactory
     * @param UpgradeData $upgradeData
     * @param AddressRepositoryInterface $addressRepository
     * @param CustomerFactory $customerFactory
     * @param StoreManagerInterface $storeManager
     * @param $mappedMagentoAttributeCodes
     */
    public function __construct(
        CustomerInterfaceFactory $customerDataInterfaceFactory,
        CustomerRepositoryInterface $customerRepositoryInterfaceFactory,
        AddressInterfaceFactory $customerAddressDataFactory,
        FailedCustomerImportFactory $failedCustomerImportFactory,
        LoggerInterface $logger,
        WebsiteFactory $websiteFactory,
        UpgradeData $upgradeData,
        AddressRepositoryInterface $addressRepository,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager,
        $mappedMagentoAttributeCodes)
    {
        $this->_logger = $logger;
        $this->_failedCustomerImportFactory = $failedCustomerImportFactory;
        $this->_customerDataFactory = $customerDataInterfaceFactory;
        $this->_customerRepositoryFactory = $customerRepositoryInterfaceFactory;
        $this->_customerAddressDataFactory = $customerAddressDataFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->_customCustomerAttributes = $upgradeData->_getLegacyCustomerAttributes();
        $this->_customCustomerAddressAttributes = $upgradeData->_getLegacyCustomerAddressAttributes();
        $this->_addressRepository = $addressRepository;
        $this->_customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->mappedMagentoAttributeCodes = $mappedMagentoAttributeCodes;

        // Include the name of the fields that will be excluded from the import.
        $this->_excludedColumns = array(
            'peppar'=>true,
            'pnr'=>true,
            'pnr2'=>true,
            'session_cookie'=>true
        );

        $this->_customerObjectAttributes = [
            'email'=>true, // if null it will not be included, as isset() will return false in that case
            'gender'=>null,
            'firstname'=>true,
            'lastname'=>true,
            'website_id'=>true, // We need to connect the legacy_server with the website_id
            'legacy_username'=>true,
            'sales_consent_contact'=>true,
            'sales_consent_mobile'=>true,
            'internal_comments'=>true,
            'legacy_konto'=>true,
            'legacy_kundid'=>true,
            'legacy_server'=>true,
            'legacy_tiderbjudepost'=>true,
            'legacy_tid_aktivitet'=>true,
            'legacy_tid_registrerad'=>true,
            'legacy_tid_uppdaterad'=>true,
            'legacy_country'=>true,
            'sum' =>true,
            'taxvat'=>true,
            'created_at'=>true
        ];
        $this->_customerAddressObjectAttributes= [
            'street'=>true,
            'street2'=>true,
            'legacy_adress2_extra'=>true,
            'legacy_adress_extra'=>true,
            'firstname'=>true,
            'lastname'=>true,
            'legacy_efternamn2'=>true,
            'legacy_email2'=>true,
            'fax'=>true,
            'company'=>true,
            'name'=>true,
            'legacy_name2'=>true,
            'country_id'=>true, // Need to map this one with Magento. SE ES DE DK FI BR
            'legacy_country_id2'=>true,
            'city'=>true,
            'legacy_city2'=>true,
            'postcode'=>true,
            'legacy_postcode2'=>true,
            'legacy_stat'=>true,
            'legacy_stat2'=>true,
            'mobile_phone'=>true,
            'telephone'=>true,
            'telephone2'=>true,
            'telephone3'=>true,
            'legacy_kundkategori_id'=>true
        ];

        $this->_landList = [
            "Norge" =>'NO',
            "Estonia"  =>'EE',
            "Latvia"  =>'LV',
            "Germany"  =>'DE',
            "France"  =>'FR',
            "Cyprus"  =>'CY',
            "Colombia"  =>'CO',
            "Slovenia"  =>'SI',
            "Luxembourg"  =>'LU',
            "Croatia"  =>'HR',
            "Czech Republic"  =>'CZ',
            "Cuba"  =>'CU',
            "Italy"  =>'IT',
            "Malta" =>'MT',
            "Greece" =>'GR',
            "Ireland" =>'IE',
            "Danmark" =>'DK',
            "Denmark" =>'DK',
            "Greenland" =>'',
            "Switzerland" =>'CH',
            "Hungary" =>'HU',
            "Belgium" =>'BE',
            "Suomi" =>'FI',
            "Lithuania" =>'LT',
            "Netherlands" =>'NL',
            "Portugal" =>'PT',
            "Romania" =>'RO',
            "Mexico" =>'MX',
            "Finland" =>'FI',
            "Faroe Islands" =>'FO',
            "España" =>'SE',
            "Norway" =>'NO',
            "Bulgaria" =>'BG',
            "Austria" =>'AT',
            "EspaÃ±a" =>'ES',
            "Poland" =>'PL',
            "Argentina" =>'AR',
            "Slovakia" =>'SK',
            "Sverige" =>'SE',
            "United Kingdom" =>'GB',
            "default"=> self::DEFAULT_COUNTRY_ID_CODE
        ];

        $this->_websiteId = [
            '1'=>'sweden',
            '2'=>'norway',
            '3'=>'finland',
            '4'=>'base',
            '5'=>'denmark',
            '6'=>'spain',
            '7'=>'netherlands',
            '8'=>'belgium',
            '9'=>'brazil'

        ];

        $this->genderMapping = [
            'k'=>'2', //Female
            'm'=>'1', //Male
            'na'=>'3' //Not Specified
        ];
    }

    /**
     * @param $attributeValue
     * @return string
     */
    public function getGenderMapping($attributeValue){
        $gender = strtolower($attributeValue);
        return (isset($this->genderMapping[$gender]))?
            $this->genderMapping[$gender]:
            $this->genderMapping['na'];
    }

    /**
     * @param $sourceAttributeCode
     * @return bool
     */
    public function getMappedMagentoAttributeCode($sourceAttributeCode)
    {
        return isset($this->mappedMagentoAttributeCodes[$sourceAttributeCode])?
            $this->mappedMagentoAttributeCodes[$sourceAttributeCode]:false;
    }

    /**
     * @param string $filename Path to file to import
     * @param array $testStore Use in tests to add test store(s) to internal array
     */
    public function import($filename=null, $testStore = [])
    {
        if($filename==null){
            throw \Exception('type the name of the CSV file containing the data.');
        }
        if(!file_exists($filename )){
            echo "Sorry, cannot open the file! \n";
            exit();
        }
        if(false === empty($testStore)) {
            $this->_websiteId = array_merge($this->_websiteId, $testStore);
        }
        $this->_import($filename);
    }

    /**
     * @param $filename
     */
    protected function _import($filename)
    {
        $_websiteModel = $this->_websiteFactory->create();
        $_websiteModelCollection = $_websiteModel->getCollection();
        $_websiteIdList = array();

        foreach ($_websiteModelCollection as $_website) {
            $_websiteIdList[$_website->getCode()]=$_website->getId();
        }

        $row = 0;
        $errors = [];
        $_bannedServers= [
            "m.swissclinic.se.marcelino.askasdrift.se" =>true,
            "swissclinic.fi.marcelino.askasdrift.se" =>true,
            "swissclinic.dk.helen.askasdrift.se" =>true,
        ];

        try{
            ini_set('auto_detect_line_endings',TRUE);
            if (($handle = fopen($filename , "r")) !== FALSE) {
                $columnNames = array();
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    // Ok so it is the first row? get the column names!
                    if(empty($columnNames)){
                        $columnNames = array_merge($columnNames,$data);
                        continue;
                    }
                    $num = count($data);
                    $customerDataModel = $this->_customerDataFactory->create();
                    $customerAddressModel = $this->_customerAddressDataFactory->create();

                    // Going forward for each column.
                    for ($c=0; $c < $num; $c++) {
                        if(isset($columnNames[$c])
                            && isset($data[$c])
                            && $this->getMappedMagentoAttributeCode(
                                $columnNames[$c]
                            )
                        ){
                            $attributeCode = $this->getMappedMagentoAttributeCode(
                                $columnNames[$c]
                            );
                            $attributeValue = $data[$c];

                            if($attributeCode =='country_id'){
                                // Map country_id with right ISO code.
                                $attributeValue = ($this->_getCountryId($attributeValue)) ?
                                    $this->_getCountryId($attributeValue) :
                                    $this->_getCountryId('default');
                            }
                            if($attributeCode =='website_id') {

                                // and now make sure to map set website_id attribute.
                                $attributeValue = ($this->_getWebsiteCode($attributeValue)) ?
                                    $this->_getWebsiteCode($attributeValue) :
                                    $_websiteIdList[self::DEFAULT_WEBSITE_CODE];
                                /* Set the store ID before anything else!
                                 * */
                                    $storeId = $this->storeManager->getWebsite($attributeValue)->getDefaultStore()->getId();
                                    $customerDataModel->setStoreId($storeId);
                            }
                            if('gender'===$attributeCode){
                                $attributeValue = $this->getGenderMapping($attributeValue);
                            }
                            /*
                             * Distribute the attributes between the two EAV data entity types Customer or Customer/Address.
                             * Some attributes are present in both EAV data entities. Such as for example; firstname or lastname
                             */

                            // Check is the attribute should be store in customer entity or customer_address entity.
                            if(isset($this->_customerObjectAttributes[$attributeCode])){
                                if('email'==$attributeCode &&
                                    $attributeValue !==''
                                ){
                                    $email = $attributeValue;
                                }
                                if(isset($this->_customCustomerAttributes[$attributeCode])){
                                    $customerDataModel->setCustomAttribute($attributeCode,$attributeValue);
                                }
                                else {
                                    $customerDataModel->setData($attributeCode,$attributeValue);
                                }
                            }

                            //Customer/address
                            if(isset($this->_customerAddressObjectAttributes[$attributeCode])){
                                // ok, is it a multi-line attribute? Do we already have a value in the first line?
                                // If so create an array with the two lines.

                                if($attributeCode=='street2'){
                                    $customerAddressDataArray = $customerAddressModel->__toArray();

                                    $attributeCode = 'street';
                                    $_firstAddressLine = isset($customerAddressDataArray['street'])?$customerAddressDataArray['street']:'';
                                    $attributeValue = [$_firstAddressLine, $attributeValue];
                                }

                                // Validation test if mandatory field phone field is empty. Set it to NA
                                if('telephone'==$attributeCode && $attributeValue ==''){
                                    $attributeValue = 'na';
                                }

                                // small fix for custom attributes.
                                if( isset($this->_customCustomerAddressAttributes[$attributeCode]) ){
                                    $customerAddressModel->setCustomAttribute($attributeCode,$attributeValue);
                                }
                                else {
                                    $customerAddressModel->setData($attributeCode,$attributeValue);
                                }
                            }
                        }
                    }
                    $customerDataArray = $customerDataModel->__toArray();
                    $_serverKey = (isset($customerDataArray['legacy_server']))? $customerDataArray['legacy_server']: false;

                    // Is this from the legacy testing servers, then skip them!
                    if(isset($_bannedServers[$_serverKey])){
                        continue;
                    }

                    // Right, now we got all the attributes, time to save the customer object.
                    try{
                        $this->_customerRepositoryFactory->save($customerDataModel);
                    }
                    catch (\Magento\Framework\Exception\AlreadyExistsException $alreadyExistsException){
                        $errors[]= $row;
                        $this->_storeFailedTransaction($alreadyExistsException,$row, $customerDataArray);
                        continue;
                    }
                    catch(\Magento\Framework\Validator\Exception $alreadyExistsException){
                        $errors[]= $row;
                        $this->_storeFailedTransaction($alreadyExistsException,$row, $customerDataArray);
                        continue;
                    }

                    // Ok, so we manage to save the customer, time to attach the address data.
                    $customer = $this->_customerFactory->create();
                    $customer->setWebsiteId($customerDataModel->getWebsiteId())->loadByEmail($customerDataModel->getEmail());

                    if($customer->getId()){
                        $customerAddressModel
                            ->setCustomerId($customer->getId())
                            ->setIsDefaultBilling('1')
                            ->setIsDefaultShipping('1');
                        try {
                            $this->_addressRepository->save($customerAddressModel);
                        } catch (\Exception $e) {
                            $this->_storeFailedTransaction($e, $row, $customerDataModel->__toArray());
                            continue;
                        }
                    }
                    $row++;
                }
                $this->_saveFailedTransactions();
                echo "Total number of processed customers: $row \n";
                $_errorCount = count($errors);
                if($_errorCount>0){
                    echo "Total of errors found: $_errorCount \n";
                }
                fclose($handle);
            }
        }
        catch(Exception $e){
            $this->_logger->critical("We got an error on processing row $row anything else here to do?");
            $this->_logger->critical($e->getMessage());
            exit();
        }
    }

    /**
     * Store a failed transaction object to save later
     *
     * @param \Exception $_exception
     * @param int $row
     * @param array $_record
     */
    protected function _storeFailedTransaction($_exception, $row, $_record)
    {
        $_failedCustomerImportModel = $this->_failedCustomerImportFactory->create();
        $this->_logger->critical("We got an error on processing row $row anything else here to do?");
        $this->_logger->critical($_exception->getMessage());
        $_jsonLogMessage = [
            'message'=>$_exception->getMessage(),
            'transactionRow'=>$row,
            'transactionData'=>$_record
        ];
        $_jsonLogMessage = json_encode($_jsonLogMessage);
        $_failedCustomerImportModel->setData('log_message',$_jsonLogMessage);
        $this->_failedTransactions[] = $_failedCustomerImportModel;
    }

    /**
     * Save stored failed transactions
     */
    protected function _saveFailedTransactions()
    {
        foreach($this->_failedTransactions as $transaction) {
            $transaction->save();
        }
    }

    /**
     * @param $country
     * @return bool|mixed
     */
    protected function _getCountryId($country)
    {
        return (isset($this->_landList[$country]))?
            $this->_landList[$country]:
            false;
    }

    /**
     * @param $server
     * @return bool|mixed
     */
    protected function _getWebsiteCode($server)
    {
        return (isset($this->_websiteId[$server]))?
            $this->_websiteId[$server]:
            false;
    }

}

