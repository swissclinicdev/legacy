<?php

namespace Swissclinic\Legacy\Model\ResourceModel\LegacyDiscountRule;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Init collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \Swissclinic\Legacy\Model\LegacyDiscountRule::class,
            \Swissclinic\Legacy\Model\ResourceModel\LegacyDiscountRule::class
        );
    }

}