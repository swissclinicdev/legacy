<?php

namespace Swissclinic\Legacy\Model\ResourceModel;


class LegacyDiscountRule extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('swissclinic_legacy_sale_rule', 'rule_id');
        $this->_isPkAutoIncrement = false;
    }
}