<?php

namespace Swissclinic\Legacy\Model\ResourceModel\FailedDiscountImport;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Init collection
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \Swissclinic\Legacy\Model\FailedDiscountImport::class,
            \Swissclinic\Legacy\Model\ResourceModel\FailedDiscountImport::class
        );
    }

}