<?php

namespace Swissclinic\Legacy\Model\ResourceModel\FailedOrdertImport;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Init collection
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \Swissclinic\Legacy\Model\FailedorderImport::class,
            \Swissclinic\Legacy\Model\ResourceModel\FailedOrderImport::class
        );
    }

}