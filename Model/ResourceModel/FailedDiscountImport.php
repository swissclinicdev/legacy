<?php

namespace Swissclinic\Legacy\Model\ResourceModel;


class FailedDiscountImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('swissclinic_legacy_failed_discount_import', 'import_transaction_id');
    }
}