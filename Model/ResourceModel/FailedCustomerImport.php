<?php

namespace Swissclinic\Legacy\Model\ResourceModel;


class FailedCustomerImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('swissclinic_legacy_failed_customer_import', 'import_transaction_id');
    }
}