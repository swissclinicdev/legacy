<?php

namespace Swissclinic\Legacy\Model\ResourceModel;


class FailedOrderImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('swissclinic_legacy_failed_order_import', 'import_transaction_id');
    }
}