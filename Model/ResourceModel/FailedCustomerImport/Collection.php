<?php

namespace Swissclinic\Legacy\Model\ResourceModel\FailedCustomerImport;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Init collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \Swissclinic\Legacy\Model\FailedCustomerImport::class,
            \Swissclinic\Legacy\Model\ResourceModel\FailedCustomerImport::class
        );
    }
}