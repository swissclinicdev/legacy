<?php
namespace Swissclinic\Legacy\Model\ResourceModel;

use Magento\Framework\App\Config\ScopeConfigInterfaceFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use Magento\UrlRewrite\Helper\UrlRewrite as UrlRewriteHelper;
use Magento\UrlRewrite\Model\UrlRewrite;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteFactory as RewriteResourceFactory;

class ImportUrls
{
    // Import status values constants
    const STATUS_SUCCESS = 1;
    const STATUS_FAILURE_UNKNOWN = 2;
    const STATUS_FAILURE_DUPLICATE = 3;
    const STATUS_FAILURE_REQUEST_FORMAT = 4;

    /**
     * @var UrlRewriteFactory
     */
    private $_urlRewriteFactory;

    /**
     * @var UrlRewriteResourceFactory
     */
    private $_rewriteResourceFactory;

    /**
     * @var UrlRewriteHelper
     */
    private $_urlRewriteHelper;

    /**
     * @var StoreManager
     */
    private $_storeManager;

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var int
     */
    private $_lastImportStatus = 0;

    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        RewriteResourceFactory $rewriteResourceFactory,
        UrlRewriteHelper $urlRewriteHelper,
        StoreManager $storeManager,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->_urlRewriteHelper = $urlRewriteHelper;
        $this->_rewriteResourceFactory = $rewriteResourceFactory;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param array $dataRow
     * @param StoreInterface $store
     * @param $basename
     * @return int 1 if successful, 0 if not successful
     */
    public function importRewrite(array $dataRow, StoreInterface $store, string $basename) {
        try {
            $this->_urlRewriteHelper->validateRequestPath($dataRow[0]);
        } catch (LocalizedException $e) {
            $this->_lastImportStatus = self::STATUS_FAILURE_REQUEST_FORMAT;
            return 0;
        }

        $rewrite = $this->_urlRewriteFactory->create();
        /* @var $rewrite UrlRewrite */

        $targetPath = $dataRow[1];

        $rewrite->setStoreId($store->getId());
        $rewrite->setIsSystem(0); // set to 0 since it's not system created
        $rewrite->setDescription(sprintf('imported_from:%s:%s_to_%s', $basename, $dataRow[0], $targetPath)); // unique value with some verbosity

        $rewrite->setTargetPath($targetPath);
        $rewrite->setRequestPath($dataRow[0]);
        $rewrite->setRedirectType(301);

        $resource = $this->_rewriteResourceFactory->create();
        /* @var $resource \Magento\UrlRewrite\Model\ResourceModel\UrlRewrite */

        try {
            $resource->save($rewrite);
        } catch (AlreadyExistsException $e) {
            $this->_lastImportStatus = self::STATUS_FAILURE_DUPLICATE;
            return 0;
        } catch (\Exception $e) {
            $this->_lastImportStatus = self::STATUS_FAILURE_UNKNOWN;
            return 0;
        }
        $this->_lastImportStatus = self::STATUS_SUCCESS;
        return 1;
    }

    /**
     * 1 = success
     * 2 = failure, reason unknown
     * 3 = failure, duplicate
     * 4 = failure, bad request path format
     * @return int
     */
    public function getLastImportStatus()
    {
        return $this->_lastImportStatus;
    }
}