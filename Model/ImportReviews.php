<?php

namespace Swissclinic\Legacy\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Review\Model\Rating;

class ImportReviews {
    const DEFAUL_RATING_CODE = 2; // VALUE rating

    const INCREASE_BY_FACTOR = 5;


    protected $_logger;
    protected $_couponRepositoryInterfaceFactory;
    protected $_couponDataInterfaceFactory;
    protected $_ruleAttributeMapping = array();
    protected $_excludedColumns = array();
    protected $_reviewFactory;
    /**
     * Rating model
     *
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $_ratingFactory;
    /**
     * Catalog product model
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    protected $_customerRepository;
    /**
     * Config model
     *
     * @var Config
     */

    protected $_configFactoryFactory;

    public function __construct(
        LegacyLoggerFactory $logger,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepository,
        ConfigFactory $configFactory

    ){
        $this->_logger = $logger;
        $this->_reviewFactory = $reviewFactory;
        $this->_excludedColumns = array();
        $this->_ruleAttributeMapping = [];
        $this->_productRepository = $productRepository;
        $this->_ratingFactory = $ratingFactory;
        $this->_customerRepository = $customerRepository;
        $this->_configFactoryFactory = $configFactory;

    }

    /**
     * @param null $filename
     */
    public function import($filename=null){
        $_result = false;

        if($filename==null){
            throw \Exception('type the name of the CSV file containing the data.');
        }
        if(!file_exists($filename )){
            echo "Sorry, cannot open the file! \n";
            exit();
        }
        if(($_resultOfImport = $this->_import($filename)) && empty($_resultOfImport['errors'])){
            $_result = true;
        }
        return $_result;
    }


    /**
     * @param $filename
     * @return array
     */
    protected function _import($filename){

        $_result = array('errors'=>array());
        $errors= array();
        $_logger = $this->_logger->create();
        $row =1;
        try{

            ini_set('auto_detect_line_endings',TRUE);

            if (($handle = fopen($filename , "r")) !== FALSE) {

                $columnNames = array();


                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    // Ok so it is the first row? get the column names!
                    if(empty($columnNames)){
                        $columnNames = array_merge($columnNames,$data);
                        continue;
                    }
                    $_columnNameDataRow = array_combine($columnNames,$data);

                    try{
                        if ( !empty($_columnNameDataRow) && ($reviewData = $this->_getReviewData($_columnNameDataRow)) && ($productId = $this->_getProductId($_columnNameDataRow['Artnr']))
                        ) {

                            // Convert the Askas data to Magento review data.

                            $review = $this->_reviewFactory->create()->setData($reviewData);
                            $review->unsetData('review_id');
                            $validate = $review->validate();

                            $_rate =  (isset($_columnNameDataRow['Betyg']))? (int) $_columnNameDataRow['Betyg'] + self::INCREASE_BY_FACTOR : self::INCREASE_BY_FACTOR;
                            $rating = array(SELF::DEFAUL_RATING_CODE => $_rate);

                            if ($validate === true) {
                                try {
                                    $_customerId = $this->_getCustomerId($_columnNameDataRow['Kund_Epost']);
                                    $_storeId = $this->_getStoreId($_columnNameDataRow['Sprak_Suffix']);
                                    echo 'Checking the store id';


                                    $review->setEntityId($review->getEntityIdByCode(\Magento\Review\Model\Review::ENTITY_PRODUCT_CODE))
                                        ->setEntityPkValue($productId)
                                        ->setStatusId(\Magento\Review\Model\Review::STATUS_PENDING)
                                        ->setCustomerId($_customerId)
                                        ->setStoreId($_storeId)
                                        ->setStores([$_storeId])
                                        ->save();
                                    foreach ($rating as $ratingId => $optionId) {
                                        $ratingObject = $this->_ratingFactory->create()
                                            ->setRatingId($ratingId)
                                            ->setReviewId($review->getId())
                                            ->setCustomerId($this->_getCustomerId($_columnNameDataRow['Kund_Epost']))
                                            ->addOptionVote($optionId, $productId)
                                        ;
                                        /* @var $ratingObject Rating */
                                        $ratingObject->save();
                                    }
                                    $review->aggregate();

                                } catch (\Exception $e) {
                                    $_logger->error($e->getMessage());
                                    $errors[]= $row;

                                    // Throw error - THIS POST can not be imported.
                                }
                            }
                            else{
                                $_logger->error('We got an error with the validation of the reviews');
                                $errors[]= $row;
                                continue;
                                // Error!
                            }
                        }
                    }

                    catch (\Magento\Framework\Exception\LocalizedException $localizedException){

                        $_logger->error($localizedException->getMessage());
                        $errors[]= $row;
                        continue;

                    }

                    $row++;
                }
                echo "Total number of processed reviews: $row \n";
                $_errorCount = count($errors);

                if($_errorCount>0){
                    echo "Total of errors found: $_errorCount \n";
                }

                fclose($handle);
            }
        }
        catch(\Exception $e){
            $_logger->critical("We got an error on processing row $row anything else here to do?");
            $_logger->critical($e->getMessage());
            $errors[]= $row;
            exit();
        }
        $_result['errors'] = $errors;

        return $_result;
    }

    protected function _getProductId($sku){
        $_return = false;

        if (!$sku) {
            return false;
        }
        try {
           If(($productId = $this->_productRepository->get($sku, $editMode = false, $storeId = null, $forceReload = true)->getId())) {
               $_return = $productId;
           }


        } catch (NoSuchEntityException $noEntityException) {
            return false;
        }
        return $_return;

    }
    protected function _getReviewData($data){

        //matching the status from Askås with Magento.
        $_reviewStatus = array(
            0 => 2, // Pending
            1 => 1 // Approved
        );

        $_sourceStatus = (int) $data['Godkant'];
        $_magentoStatus = (isset($_reviewStatus[$_sourceStatus]))? $_reviewStatus[$_sourceStatus] : 2;
        $_nickname = (string) $data['Kund_Namn'];
        $_text = (string) $data['Text'];
        $_stores = array();

        /*  <option value="1">Approved</option>
            <option value="2" selected="selected">Pending</option>
            <option value="3">Not Approved</option> */



        $_reviewData = array(
            'status_id'=> $_magentoStatus,
            'stores'=> $_stores,
            'nickname' => $_nickname,
            'title' => $_text,
            'detail' => $_text
        );
        /*
         * ratings[2]
         * status_id,
         * stores []
         * nickname
         * title
         * detail
         *
         * */

        return $_reviewData;
    }

    /**
     * @param $email
     * @return null
     */
    protected function _getCustomerId($email){
        $_logger = $this->_logger->create();
        $_return = null;
        $customerRepo = $this->_customerRepository->create();

        try {
            if(($customer = $customerRepo->get($email)) && $customer->getId()){
                $_return = $customer->getId();
            }

        }
        catch (\Exception $exception){
            $_logger->info($exception->getMessage());
        }


        return $_return;
    }

    /**
     * @param $_countryId
     * @return bool
     */
    protected function _getStoreId($_countryId){
        $_return = false;
        if (
            ($_config = $this->_configFactoryFactory->create())
            && ($_storeId = $_config->getWebsiteModel(strtolower($_countryId))->getDefaultStore()->getId())) {
            $_return = $_storeId;
        }
        return $_return;
    }


}

