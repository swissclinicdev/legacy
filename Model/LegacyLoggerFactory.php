<?php


namespace Swissclinic\Legacy\Model;

/**
 * Class to get PSR-3 compliant logger instance
 */
class LegacyLoggerFactory
{
    private $_channelName = 'legacy-logger';
    private $_logFilename = 'error_legacy_import.log';

    public function setChannelName($channelName)
    {
        $this->_channelName = $channelName;
        return $this;
    }

    public function setLogFilename($logFilename)
    {
        $this->_logFilename= $logFilename;
        return $this;
    }

    public function create()
    {
        $logger = new \Monolog\Logger($this->_channelName);
        $path = BP . '/var/log/' . $this->_logFilename;
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($path));
        return $logger;
    }
}
