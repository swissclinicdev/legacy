<?php

namespace Swissclinic\Legacy\Model;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Store\Model\WebsiteFactory;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class ImportOrders {

    protected $columnNames;
    protected $_logger;
    protected $_excludedColumns;
    protected $_orderObjectAttributes;
    protected $_landList;
    protected $_websiteId;
    protected $_websiteCollection;
    protected $_failedOrderImportFactory;
    protected $_websiteFactory;
    protected $_orderAddressObjectAttributes;
    protected $_customerRepo;
    protected $_cartManagementInterface;
    protected $_cartRepoInterface;
    protected $_productRepo;
    protected $_shippingRateFactory;



    const DEFAULT_COUNTRY_ID_CODE = 'SE';
    const DEFAULT_WEBSITE_CODE ='sweden';


    public function __construct(
        \Swissclinic\Legacy\Model\FailedOrderImportFactory $failedOrderImportFactory,
        LoggerInterface $logger,
        \Swissclinic\Legacy\Setup\UpgradeSchema $upgradeSchema,
        CustomerRepositoryInterface $customerRepo,
        CartManagementInterface $cartManagementInterface,
        CartRepositoryInterface $cartRepoInterface,
        WebsiteFactory $websiteFactory,
        ProductRepositoryInterface $productRepoInterface,
        \Magento\Quote\Model\Quote\Address\RateFactory $_shippingRateFactory

    ){
        $this->_logger = $logger;
        $this->_failedOrderImportFactory = $failedOrderImportFactory;
        $this->_customerRepo      = $customerRepo;
        $this->_cartManagementInterface  = $cartManagementInterface;
        $this->_cartRepoInterface = $cartRepoInterface;
        $this->_websiteFactory = $websiteFactory;
        $this->_productRepo = $productRepoInterface;
        $this->_shippingRateFactory = $_shippingRateFactory;

        // Include the name of the fields that will be excluded from the import.
        $this->_excludedColumns = array(
            'peppar'=>true,
            'pnr'=>true,
            'pnr2'=>true,
            'session_cookie'=>true
        );


        $this->_orderObjectAttributes = [
            'email'=>true, // if null it will not be included, as isset() will return false in that case
            'currency_id'=>true,
            'discount_code'=>true,
            'legacy_cart_rule_id'=>true, // This one is
            'country_id'=>true,
            'paymentMethod'=> true,
            'items'=> true
        ];

        $this->_orderAddressObjectAttributes= [
            'firstname'=>true,
            'lastname'=>true,
            'street'=>true,
            'city'=>true,
            'country_id'=>true,
            'region'=>true,
            'postcode'=>true,
            'telephone'=>true,
        ];
        $this->_orderItemObjectAttributes = [
            'sku'=>true,
            'qty'=>true,
            'price'=>true
        ];

        $this->_landList = [
            "Norge" =>'NO',
            "Estonia"  =>'EE',
            "Latvia"  =>'LV',
            "Germany"  =>'DE',
            "France"  =>'FR',
            "Cyprus"  =>'CY',
            "Colombia"  =>'CO',
            "Slovenia"  =>'SI',
            "Luxembourg"  =>'LU',
            "Croatia"  =>'HR',
            "Czech Republic"  =>'CZ',
            "Cuba"  =>'CU',
            "Italy"  =>'IT',
            "Malta" =>'MT',
            "Greece" =>'GR',
            "Ireland" =>'IE',
            "Danmark" =>'DK',
            "Denmark" =>'DK',
            "Greenland" =>'',
            "Switzerland" =>'CH',
            "Hungary" =>'HU',
            "Belgium" =>'BE',
            "Suomi" =>'FI',
            "Lithuania" =>'LT',
            "Netherlands" =>'NL',
            "Portugal" =>'PT',
            "Romania" =>'RO',
            "Mexico" =>'MX',
            "Finland" =>'FI',
            "Faroe Islands" =>'FO',
            "España" =>'SE',
            "Norway" =>'NO',
            "Bulgaria" =>'BG',
            "Austria" =>'AT',
            "EspaÃ±a" =>'ES',
            "Poland" =>'PL',
            "Argentina" =>'AR',
            "Slovakia" =>'SK',
            "Sverige" =>'SE',
            "United Kingdom" =>'GB',
            "default"=> self::DEFAULT_COUNTRY_ID_CODE
        ];


        $this->_websiteId= array(
            "m.swissclinic.se.marcelino.askasdrift.se" =>'sweden',
            "www.swissclinic.co.uk"  =>'greatbritain',
            "m.swissclinic.fi" =>'finland',
            "www.swissclinic.se" =>'sweden',
            "www.swissclinic.be" =>'belgium',
            "www.swissclinic.fi" =>'finland',
            "swissclinic.fi.marcelino.askasdrift.se" =>'sweden',
            "swissclinic.dk.helen.askasdrift.se" =>'sweden',
            "m.swissclinic.dk" =>'denmark',
            "www.swissclinic.no" =>'norway',
            "www.swissclinic.nl" =>'netherlands',
            "m.swissclinic.no" =>'norway',
            "m.swissclinic.se" =>'sweden',
            "www.swissclinic.dk" =>'denmark',
            "www.swissclinic.es" =>'spain',
            'default'=>self::DEFAULT_WEBSITE_CODE
        );

    }

    /**
     * @param string $filename Path to file to import
     * @param array $testStore Use in tests to add test store(s) to internal array
     */
    public function import($filename=null, $testStore = []) : array{

        if($filename==null){
            throw \Exception('type the name of the CSV file containing the data.');
        }
        if(!file_exists($filename )){
            echo "Sorry, cannot open the file! \n";
            exit();
        }
        if(false === empty($testStore)) {
            $this->_websiteId = array_merge($this->_websiteId, $testStore);
        }
        $return = $this->_import($filename);
        return $return;
    }

    /**
     * @param $string
     * @param $separator
     * @return array
     */
    protected function _getItems($stringValue, $separator) : array{
        $result = explode($separator,$stringValue);
        return $result;
    }


    /**
     * @param $orderData
     * @return mixed
     */
    public function createOrder($orderData): string{

        if(($cartId = $this->_cartManagementInterface->createEmptyCart())
            && ($cart = $this->_cartRepoInterface->get($cartId))
            && ($store = $this->_getStore($orderData['website_code']))
            && $store
        ){
            /**
             * TODO: We need here the quote object to have the right information, that is currency and and store info.
             */

             $cart->setStore($store);
             $cart->setCurrency();
            // Get stock now

        }
        else {
            return false;
        }
        try {

           // check if the order was sent by a guest customer or not!
           $customerEntity = $this->_customerRepo->get($orderData['email'], $orderData['website_code']);
           $customerId = $this->_customerRepo->getById($customerEntity->getId());


           $cart->assignCustomer($customerId);

        }
       catch (NoSuchEntityException $e) {

           // Ok, so it was created as a guest!

           $cart->setCheckoutMethod(CartManagementInterface::METHOD_GUEST);
           $cart->getBillingAddress()->setEmail($orderData['email']);

       }

        // So time to set some products
        foreach ($orderData['items'] as $item) {
            $product = $this->_productRepo->get($item[0]);
            // Get the price here, as the addProduct method, does not have arguments for passing custom price.
            $product->setPrice($item[1]);
            // The system takes care of checking if the items are available in Magento. "Requested product doesn't exist"


                $cart->addProduct(
                $product,
                $item[2]
            );



        }

        // Right, now that we have added the items to cart. It is time to set the address.
        $cart->getBillingAddress()->addData($orderData['billing_address']);
        $cart->getShippingAddress()->addData($orderData['shipping_address']);


        // Now is time to set the shipping method!
        $cart->getShippingAddress()->setShippingMethod('flatrate_flatrate');

        $_shippingRate = $this->_shippingRateFactory->create()->setCode($cart->getShippingAddress()->getShippingMethod());
        $cart->getShippingAddress()->addShippingRate($_shippingRate);
        $cart->getShippingAddress()->save();

        // Now, it is time to work with the discounts.
        if(isset($orderData['payment_method']) &&
            ($paymentMethod = (string)$orderData['payment_method'] )){
            // Set payment method
            $cart->setPaymentMethod($paymentMethod);
            $cart->getPayment()->importData(['method' => $paymentMethod]);
        }

        // Due to that we
        //OK, Now we got the totals.
        $cart->collectTotals();
        $cart->save();

        //And now that everything has been passed, time to create the order.
        $cart = $this->_cartRepoInterface->get($cart->getId());
        $orderId = $this->_cartManagementInterface->placeOrder($cart->getId());

        return $orderId;

    }

    /**
     * @param $_websiteCode
     * @return bool
     */
    protected function _getStore($_websiteCode){


        if( ($_websiteModel = $this->_websiteFactory->create()) &&
            ($_websiteModel = $_websiteModel->load($_websiteCode)) &&
            ($_defaultStore = $_websiteModel->getDefaultStore())
        ){
            return $_defaultStore;
        }
        return false;
    }

    /**
     * @return array
     */
    protected function _getOrderData() : array{

        $_orderData = array(
            'email'=> '',
            'store' => array(),
            'website_code'=>'',
            'currency_id'=>'',
            'items'=>array(),
            'shipping_address'=> array(),
            'billing_address'=> array(),
            'legacy_cart_rule_id'=>'',
            'discount_code'=>'',
            'payment_method'=>''
        );

        return $_orderData;
    }




    /**
     * @param $server
     * @return bool|mixed
     */
    protected function _getWebsiteCode($server){
        $return = (isset($this->_websiteId[$server]))?
            $this->_websiteId[$server]:
            false;
        return $return;
    }

    /**
     * @param $_orderData
     * @param $key
     * @param $value
     * @return array
     */
    protected function setOrderData ($_orderData, $key, $value =''):array{

        if(is_array($key)){
            $_orderData=  array_replace_recursive($key,$_orderData);
        }
        else {
            $_orderData[$key] = $value;
        }
        return $_orderData;
    }

    protected function getAttributeValueAndCode($_attributeCode, $_needle, $attributeValue) : array{

        $_fieldname = substr( $_attributeCode, strlen($_needle));
        if('country_id'== $_fieldname){
            $attributeValue = $this->_getCountryId($attributeValue);
        }
        $_pathAndValue = array(substr($_needle, 0, -1) => array( $_fieldname => $attributeValue));
        return $_pathAndValue;

    }

    /**
     * @param $filename
     */
    protected function _import($filename){

        $_result= array(
            'errors'=> array(),
            'successfull'=> array()
        );

        $_websiteModel = $this->_websiteFactory->create();
        $_websiteModelCollection = $_websiteModel->getCollection();
        $_websiteIdList = array();

        foreach ($_websiteModelCollection as $_website) {
            $_websiteIdList[$_website->getCode()]=$_website->getId();
        }

        $row = 0;
        $errors = [];
        $_bannedServers = [
            "m.swissclinic.se.marcelino.askasdrift.se" =>true,
            "swissclinic.fi.marcelino.askasdrift.se" =>true,
            "swissclinic.dk.helen.askasdrift.se" =>true,
        ];

        try{

            ini_set('auto_detect_line_endings',TRUE);

            if (($handle = fopen($filename , "r")) !== FALSE) {

                /*
                 *
                 * The idea is that the csv file should look as follow:
                 *
                 * email, payment_method, currency_id, discount_code, legacy_cart_rule_id, server, items, billing_address, shipping_address
                 * "blabla@blabla.com","klarna","SEK","hyd77373","5","SE","sku;price;qty","firstname","lastname","street","street2","city", "region","postcode","country_id","firstname","lastname","street","street2","city", "region,postcode","country_id"
                 * "test@test.com","checkmo","SEK","testcode","5","SE","t234;450;2","t123;120;5","James","Bond","Bond street","","London","","WC2N 5DU,UK","James",Bond","The other street","","London","","WC2N 5DU,UK"
                 */


                $columnNames = array();
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    // Ok so it is the first row? get the column names!
                    if(empty($columnNames)){
                        $columnNames = array_merge($columnNames,$data);
                        continue;
                    }
                    $num = count($data);

                    // Get the an empty orderData array!
                    $orderData = $this->_getOrderData();

                    // Going forward for each column.
                    for ($c=0; $c < $num; $c++) {

                        if(isset($columnNames[$c])
                            && isset($data[$c])
                            && !isset($this->_excludedColumns[$columnNames[$c]])){

                            $attributeCode = trim($columnNames[$c]);
                            $attributeValue = $data[$c];


                            if('server' === $attributeCode){

                                // So based on the server we'll get the website_code.
                              if(($_websiteCode = $this->_getWebsiteCode($attributeValue))
                                  && $_websiteCode){

                                  $attributeCode = 'website_code';
                                  $attributeValue = $_websiteCode;
                                  $orderData = $this->setOrderData($orderData, $attributeCode, $attributeValue);
                              }
                              else {
                                  $phrase=  new \Magento\Framework\Phrase('We got an issue with the order in row ' . $row .' The website code was not mapped!');
                                  throw new \Magento\Framework\Exception\LocalizedException($phrase);
                              }
                            }

                            // Ok, so we got an items column here? That means that it contains items inside.sku;price;qty
                           else if('items'===$attributeCode){

                                $_itemList = array();
                                $_items = $this->_getItems($attributeValue,',');

                                // Now for each item line we need to extract the attributes. This are qty, price and sku.

                                foreach ($_items as $_item){
                                    $_item =  $this->_getItems($_item,';');
                                    $_itemList[] = $_item;
                                }

                                // Now that we got the list, time to move it to the product list.
                               $attributeValue = $_itemList;
                               $orderData = $this->setOrderData($orderData, $attributeCode, $attributeValue);

                                if(count($attributeValue)<0){
                                    $phrase=  new \Magento\Framework\Phrase('We got an issue with the order in row ' . $row .' No items were found!');
                                    throw new \Magento\Framework\Exception\LocalizedException($phrase);
                                }

                            }

                            else if(( $_needle = 'shipping_address_') &&
                                strpos($attributeCode,$_needle,0) !==false)
                            {

                                $_attributeValueAndCode= $this->getAttributeValueAndCode($attributeCode,$_needle,$attributeValue);
                                $orderData = $this->setOrderData($orderData, $_attributeValueAndCode, '');
                            }

                            else if(( $_needle = 'billing_address_') &&
                                strpos($attributeCode,$_needle,0) !==false
                            ){
                                $_attributeValueAndCode= $this->getAttributeValueAndCode($attributeCode,$_needle,$attributeValue);
                                $orderData = $this->setOrderData($orderData, $_attributeValueAndCode, '');
                            }
                            else if(in_array($attributeCode, $this->_orderObjectAttributes))
                            {
                                $orderData = $this->setOrderData($orderData, $attributeCode,$attributeValue);

                            }

                        }
                    }

                    // Right, now we got all the attributes, time to save the order object.
                    try{

                        // Now that we have the product data array up and running, it is time to create the order in marvellous Magento!
                        $_order_id = $this->createOrder($orderData);

                     /*   if(!$_order_id){
                            $phrase=  new \Magento\Framework\Phrase('We got an issue with the order in row ' . $row);
                            throw new \Magento\Framework\Exception\LocalizedException($phrase);
                       }
                     */

                    }
                    catch (\Magento\Framework\Exception\AlreadyExistsException $alreadyExistsException){
                        $errors[]= $row;
                        $this->_storeFailedTransaction($alreadyExistsException,$row, $orderData);
                        continue;
                    }
                    catch(\Magento\Framework\Validator\Exception $alreadyExistsException){


                        $errors[]= $row;
                        $this->_storeFailedTransaction($alreadyExistsException,$row, $orderData);
                        continue;
                    }
                   /*  TODO: Catch this error code.

                        catch(\Magento\Framework\Exception\LocalizedException $localizedException){
                        $errors[]= $row;
                        $this->_storeFailedTransaction($localizedException,$row, $orderData);
                        continue;
                    } */

                    // Ok, so we manage to save the customer, time to attach the address data.
                    $row++;
                }

                $_result['processed'] = $row;
                $_errorCount = count($errors);

                if($_errorCount>0){
                    $_result['errors']= $_errorCount;
                }
                fclose($handle);
            }
        }
        catch(Exception $e){

            $this->_logger->critical("We got an error on processing row $row anything else here to do?");
            $this->_logger->critical($e->getMessage());
            exit();
        }
        return $_result;

    }



    /**
     * @param $_exception
     * @param $row
     * @param $_record
     */
    protected function _storeFailedTransaction($_exception, $row, $_record){

        $_failedOrderImportModel = $this->_failedOrderImportFactory->create();
        $this->_logger->critical("We got an error on processing row $row anything else here to do?");
        $this->_logger->critical($_exception->getMessage());


        $_jsonLogMessage = [
            'message'=>$_exception->getMessage(),
            'transactionRow'=>$row,
            'transactionData'=>$_record
        ];
        $_jsonLogMessage = json_encode($_jsonLogMessage);



        $_failedOrderImportModel
            ->setData('log_message',$_jsonLogMessage)
            ->save();
    }

    /**
     * @param $country
     * @return bool|mixed
     */
    protected function _getCountryId($country){
        $return = (isset($this->_landList[$country]))?
            $this->_landList[$country]:
            false;
        return $return;
    }
}