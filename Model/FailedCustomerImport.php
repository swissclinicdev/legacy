<?php

namespace Swissclinic\Legacy\Model;

class FailedCustomerImport  extends \Magento\Framework\Model\AbstractModel  {

    protected function _construct() {
        $this->_init(\Swissclinic\Legacy\Model\ResourceModel\FailedCustomerImport::class);
    }

}