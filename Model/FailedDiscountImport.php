<?php

namespace Swissclinic\Legacy\Model;

class FailedDiscountImport  extends \Magento\Framework\Model\AbstractModel  {

    protected function _construct() {
        $this->_init(\Swissclinic\Legacy\Model\ResourceModel\FailedDiscountImport::class);
    }

}