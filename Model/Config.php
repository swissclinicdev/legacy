<?php

namespace Swissclinic\Legacy\Model;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Model\Data\Rule;

class Config {

    const BESKR = 'beskr_';
    const MIN_FLOAT_VALUE = 0.00;
    const GREATER_THAN = '>';
    const CUSTOMER_GROUP_GENERAL= '1'; // General

    protected $_landListGroupsFromSource;
    protected $_websiteId;
    protected $_websiteCollection;
    protected $_websiteFactory;
    protected $_logger;



    /**
     * AttributeHandlers constructor.
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_logger = $logger;
        $this->_websiteFactory = $websiteFactory;

        // Mapping of code of source server where the rule was created and the countries where should be applied.
        $this->_landListGroupsFromSource = [
            'SV'=> array('se','no','fi','gb'),
            'DK'=> array('dk'),
            'ES'=> array('es'),
            'NL'=> array('nl','be'),
            'BR'=> array('br')
        ];

        // Mapping of iso code from source with website code in Magento.
        $this->_websiteId= array(
            'se' =>'sweden',
            'sv' =>'sweden',
            'gb' =>'greatbritain',
            'fi' =>'finland',
            'be' =>'belgium',
            'dk' =>'denmark',
            'no' =>'norway',
            'nl' =>'netherlands',
            'es' =>'spain',
            'br'=>'brazil',
            'base' => 'base' // Used during integration tests
        );
    }

    public function getWebsiteModel($_countryId){

        $_countryId = strtolower($_countryId);
        $_return = false;

        if(!isset($this->_websiteId[$_countryId])){
            // No website code? Abort!
            return false;
        }
        if(($_websiteModel = $this->_websiteFactory->create()) && ($_strWebsiteCode = $this->_websiteId[$_countryId]) && ($_websiteModel->load($_strWebsiteCode)) ){
            $_return = $_websiteModel;
        }
        return $_return;
    }


}

