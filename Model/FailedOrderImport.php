<?php

namespace Swissclinic\Legacy\Model;

class FailedorderImport  extends \Magento\Framework\Model\AbstractModel  {

    protected function _construct() {
        $this->_init(\Swissclinic\Legacy\Model\ResourceModel\FailedOrderImport::class);
    }

}