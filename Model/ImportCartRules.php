<?php

namespace Swissclinic\Legacy\Model;
use Magento\SalesRule\Api\Data\RuleInterface;
use \Magento\SalesRule\Model\Data\Rule;


class ImportCartRules {


    protected $_logger;
    protected $_excludedColumns;
    protected $_ruleAttributeMapping;
    protected $_websiteCollection;
    protected $_ruleDataFactory;
    protected $_ruleRepositoryFactory;
    protected $_conditionDataInterfaceFactory;
    protected $_ruleConditionAddressFactory;
    protected $_websiteFactory;
    protected $_customRulesAttributes;
    protected $_ruleFactory;
    protected $_ruleLabelDataInterfaceFactory;
    protected $_attributeHandlersFactory;
    protected $_legacyDiscountRuleFactory;
    protected $_failedDiscountImportFactory;

    public function __construct(
        \Magento\SalesRule\Api\Data\RuleInterfaceFactory $ruleDataInterfaceFactory,
        \Magento\SalesRule\Api\Data\RuleLabelInterfaceFactory $ruleLabelDataInterfaceFactory,
        \Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepositoryInterfaceFactory,
        \Magento\SalesRule\Api\Data\ConditionInterfaceFactory $conditionDataInterfaceFactory,
        \Magento\SalesRule\Model\Rule\Condition\AddressFactory $ruleConditionAddressFactory,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Psr\Log\LoggerInterface $logger,
        \Swissclinic\Legacy\Model\AttributeHandlersFactory $attributeHandlers,
        \Swissclinic\Legacy\Model\LegacyDiscountRuleFactory $legacyDiscountRuleFactory,
        \Swissclinic\Legacy\Model\FailedDiscountImportFactory $_failedDiscountImportFactory

    ){
        $this->_logger = $logger;
        $this->_ruleDataFactory = $ruleDataInterfaceFactory;
        $this->_ruleRepositoryFactory = $ruleRepositoryInterfaceFactory;
        $this->_ruleLabelDataInterfaceFactory = $ruleLabelDataInterfaceFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->_conditionDataInterfaceFactory = $conditionDataInterfaceFactory;
        $this->_ruleConditionAddressFactory = $ruleConditionAddressFactory;
        $this->_attributeHandlersFactory = $attributeHandlers;
        $this->_legacyDiscountRuleFactory = $legacyDiscountRuleFactory;
        $this->_failedDiscountImportFactory = $_failedDiscountImportFactory;

        // Include the name of the fields that will be excluded from the import.
        $this->_excludedColumns = array(
            'autoinlosen'=>true, // Askås "Aktivera automatisk inlösen i Kassan"
            'autoinlosen_begransasprak'=>true,
            'forbrukad'=>true,
            'begransasprak'=>true, // Will be used later on calling the handler for isocountry.
            'beskr_en'=>true, // store_labels will be handled by the name handler and store label handler.
            'beskr_fi'=>true, // store_labels
            'beskr_no'=>true, // store_labels
            'beskr_sv'=>true, // store_labels
            'beskr_dk'=>true, // store_labels
            'beskr_es'=>true, // store_labels
            'beskr_nl'=>true, // store_labels
            'engangskod'=>true,
            'georderrabatt_grans'=>true, // will be handled by condition.
            'georderrabatt_grans_artiklar'=>true, // will be handled by condition.
            'rabatt_foralder'=>true
        );


        $this->_ruleAttributeMapping = [
            'begransasprak'=>'website_ids', // string with comma separated values 'SV','NO','FI' NL

            // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal'=>  true, /*
                                       N (Obegränsat antal i varukorgen)|
                                       Y (Begränsa till) | If this one is active field "rabattkod_Begransa_Antal_Till" will be used.
                                       A (Begränsa till artikel nr ) If this one is active fields "rabattkod_Begransa_Antal_Start" and "rabattkod_Begransa_Antal_Stopp"
                                    */
            // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal_start'=>true, // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal_stopp'=>true, // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal_till'=>true, // Depends on begransa_antal = Y FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.

            'beskr_intern'=>'name',

            'beskr_en'=>'store_labels', // store_labels
            'beskr_fi'=>'store_labels', // store_labels
            'beskr_no'=>'store_labels', // store_labels
            'beskr_sv'=>'store_labels', // store_labels
            'beskr_dk'=>'store_labels', // store_labels
            'beskr_es'=>'store_labels', // store_labels
            'beskr_nl'=>'store_labels', // store_labels

            'engangskod'=>true, // This is used by the child codes. 0 | 1
            'extra_datumbegransning_start'=>'from_date', // fromDate 1381356000
            'extra_datumbegransning_stopp'=>'to_date', // toDate 1381356000
            //'forbrukad' =>true, // 0 Not used in the parents found
            'fraktfritt' =>'simple_free_shipping', // SimpleFreeShipping Value in Askås are: Y|N

            'georderrabatt' =>'condition', // In Askås is Skall kampanjen berättiga till en total orderrabatt? With option N | Y
            'georderrabatt_begransakund' =>'customer_group_ids',

                /*

                In Askås this is named, Begränsa erbjudandet till en gång per kund?
                With option to:

                Y (Ja, en gång per kundnr) |
                Y2 (Ja, en gång per kundnr och/eller ip-adress)|
                N (Nej)

                */

            'georderrabatt_begransa_artikelgrupp' =>true,
                /*

                In Askås is named, Begränsa vilka artiklar som ska ingå i totalsumma?
                This one is of type string, on the frontend is presented as a multiple choose input,
                containing integers that link to work group, such as;
                190 for the article group, 219 for beauty treatments or 201 Dieter... and so on.

                Is dependent of "GeOrderRabatt_Begransa_Artikelgrupp_Typ"  with options 1 (Inkludera) 2 (Exkludera)
                Depending of the options the selected ids stored in georderrabatt_begransa_artikelgrupp will be exluded or included.
                Here we have the first link with the table: swissclinic_rabattkoder_begransningar. Which connects the id of the rule or campaign with with "Typ".

                Rabatt_ID,"Field","Data","Typ"
                624014,"GeOrderRabatt_Begransa_Artikelgrupp","'191'","Allow".

                */
            'georderrabatt_grans' =>true, // In Askås is named "Vid köp över, SEK inkl. moms" The values is of type float.
            'georderrabatt_grans_artiklar' =>true, // In Askås is named, och minst antal artiklar i varukorgen, with values of type integer. 1 2 3 and so on.
            'georderrabatt_procent' =>'action',
            /* In Askås, is named Totalrabatt i procent. Of type float, 0.00 -
            However needs to have GeOrderRabatt_Typ set to 2 (Totalrabatt i procent). As it looks this column is not available for export
             from the source so in this case we'll make sure that if the field is the we should use this information.
            For cases in which both georderrabatt_procent and georderrabatt_summa are set we should require manual check.
             */

            'georderrabatt_summa'=>'action',
            /*
             In Askås is named Totalrabatt, SEK inkl. moms, with values of type float, for example "360.00".
             As it looks this column is not available for export
             from the source so in this case we'll make sure that if the field is the we should use this information. For cases in which both georderrabatt_procent and
             georderrabatt_summa are set we should require manual check
            */

            'rabatt_foralder'=>true, // will always be 0 for parents, single use codes will have the rule id.
            'rabatt_id'=>'legacy_rabatt_id', // legacy_rabatt_id
            'rabattkod'=>'legacy_rabattkod', // This one should only is only used on coupon codes. However on parents with child it should be stored in legacy_rabattkod
            'tid'=>true, // This one is used not available via Magento\SalesRule\Api\Data\RuleInterface - however it should be saved in a separate table for future use + record keeping. legacy_tid
            'tid_uppdaterad'=>true, // This one is used not available via Magento\SalesRule\Api\Data\RuleInterface - however it should be saved in a separate table for future use + record keeping. legacy_tid_uppdaterad
            'isocountry'=>'website_ids' // Will be used for handling the different websites in combination with begransasprak, isoCountry will have higher priority and will set the countries were the rule will apply.
        ];
    }

    /**
     * @param null $filename
     */
    public function import($filename=null){
        if($filename==null){
            throw \Exception('type the name of the CSV file containing the data.');
        }
        if(!file_exists($filename )){
            echo "Sorry, cannot open the file! \n";
            exit();
        }
        $this->_import($filename);
    }

    protected function _import($filename){

        $_websiteModel = $this->_websiteFactory->create();
        $_websiteModelCollection = $_websiteModel->getCollection();
        $_websiteIdList = array();

        foreach ($_websiteModelCollection as $_website) {
            $_websiteIdList[$_website->getCode()]=$_website->getId();
        }
        $row = 0;
        $errors = [];

        try{
            ini_set('auto_detect_line_endings',TRUE);

            if (($handle = fopen($filename , "r")) !== FALSE) {

                $columnNames = array();
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    // Ok so it is the first row? get the column names!
                    if(empty($columnNames)){
                        $columnNames = array_merge($columnNames,$data);
                        continue;
                    }

                    $_columnNameDataRow = array_combine($columnNames,$data);

                    $ruleDataModel = $this->_getRuleDataModel($_columnNameDataRow);

                    $ruleDataArray = $ruleDataModel->__toArray();


                    // Right, now we got all the attributes, time to save the customer object.
                    try{
                        // TODO: Make sure that if the fields that are supposed to not be used contain some value, we should make sure that those transactions should be stored in a table where the users should make a manual search.
                        $_ruleDataModel = $this->_ruleRepositoryFactory->save($ruleDataModel);

                        $_magento_rule_id = $_ruleDataModel->getRuleId();
                        $this->_storeLegacyDiscountData($_magento_rule_id, $_columnNameDataRow);
                    }
                    catch (\Exception $alreadyExistsException){
                        $errors[]= $row;
                        $this->_storeFailedTransaction($alreadyExistsException,$row, $ruleDataArray);
                        continue;
                    }
                    catch(\Magento\Framework\Validator\Exception $avalidatorException){
                        $errors[]= $row;
                         $this->_storeFailedTransaction($avalidatorException,$row, $ruleDataArray);
                        continue;
                    }

                    $row++;
                }
                echo "Total number of processed sale rules: $row \n";
                $_errorCount = count($errors);

                if($_errorCount>0){
                    echo "Total of errors found: $_errorCount \n";
                }

                fclose($handle);
            }
        }
        catch(Exception $e){
            $this->_logger->critical("We got an error on processing row $row anything else here to do?");
            $this->_logger->critical($e->getMessage());
            exit();
        }
    }

    protected function _storeLegacyDiscountData($ruleId, array $data){

        if(!isset($data['rabatt_id'])){ return false;}

        $_legacyDiscountRuleModel = $this->_legacyDiscountRuleFactory->create();
        $_legacyDiscountRuleModel
            ->setData('rule_id',$ruleId)
            ->setData('rabatt_id',$data['rabatt_id'])
            ->setData('rabattkod',$data['rabattkod'])
            ->setData('tid',$data['tid'])
            ->setData('tid_uppdaterad',$data['tid_uppdaterad'])
            ->save();
    }

    /**
     * @param $data
     * @return array
     */
    protected function _getRuleDataRow(array $dataRow){
        $_attributesRequiringManualCheck = [
            'georderrabatt_begransa_artikelgrupp'=>true
        ];
        $_arrRuleData = [
            Rule::KEY_NAME => '',
            Rule::KEY_STORE_LABELS => array(),
            Rule::KEY_DESCRIPTION=> '',
            Rule::KEY_WEBSITES  =>array(),
            Rule::KEY_CUSTOMER_GROUPS =>array(), // This one is never set.
            Rule::KEY_FROM_DATE =>'',
            Rule::KEY_TO_DATE =>'',
            Rule::KEY_USES_PER_CUSTOMER =>'',
            Rule::KEY_IS_ACTIVE =>'',
            Rule::KEY_CONDITION =>'',
            Rule::KEY_ACTION_CONDITION =>'',
            Rule::KEY_STOP_RULES_PROCESSING =>'',
            Rule::KEY_IS_ADVANCED =>true,
            Rule::KEY_PRODUCT_IDS =>'',
            Rule::KEY_SORT_ORDER =>'',
            Rule::KEY_SIMPLE_ACTION =>'',
            Rule::KEY_DISCOUNT_AMOUNT =>'',
            Rule::KEY_DISCOUNT_QTY =>'',
            Rule::KEY_DISCOUNT_STEP =>'',
            Rule::KEY_SIMPLE_FREE_SHIPPING =>'',
            Rule::KEY_USES_PER_COUPON =>'',
            Rule::KEY_USE_AUTO_GENERATION =>true, // default setting
            Rule::KEY_COUPON_TYPE => RuleInterface::COUPON_TYPE_SPECIFIC_COUPON, // default setting
            Rule::KEY_IS_RSS=>'',
            Rule::KEY_TIMES_USED =>'',

        ];


        // Going forward for each column.

        foreach ($dataRow as $_columnName => $_value){

            $_attributeHandlersModel = $this->_attributeHandlersFactory->create();

            if( !isset($this->_excludedColumns[$_columnName])){

                /*
                 * Let's update the rule data object with the data retrieved from the rows found in the csv file.
                 */

                if(isset($this->_ruleAttributeMapping[$_columnName]) &&
                    ($magentoAttributeName = $this->_ruleAttributeMapping[$_columnName])){

                    // Here we catch all attributes that if set
                    if(isset($_attributesRequiringManualCheck[$_columnName]) && $_value ){
                        // TODO: STORE THIS TRANSACTION SUch as for example georderrabatt_begransa_artikelgrupp
                        continue;
                    }

                    if(isset($_arrRuleData[$magentoAttributeName])){


                        // Ok, so if the method exists it can be later on be called and will convert the data retrieved into something that can be used by Magento.
                        if(
                            method_exists($_attributeHandlersModel, $magentoAttributeName) &&
                            $_arrRuleData = $_attributeHandlersModel->$magentoAttributeName(
                                $_arrRuleData, $dataRow
                            )
                        ){
                            if(!$_arrRuleData){
                                // oups, an error here!!! Store this transaction for manual check.
                                // TODO: call the transaction logger. This has to be saved.
                                continue;
                            }
                        }
                        else {
                            throw new \Exception("We dont have any method defined for handling the $magentoAttributeName attribute ");
                        }
                    }
                }
            }
        }

        return $_arrRuleData;
    }



    /**
     * @param $_arrRuleData
     * @return mixed
     */
    protected function _getRuleDataModel($_arrRuleData){

        $_arrRuleData = $this->_getRuleDataRow($_arrRuleData);
        $ruleDataModel = $this->_ruleDataFactory->create();
        foreach ($_arrRuleData as $ruleAttributeName => $ruleAttributeValue){
            $ruleDataModel->setData($ruleAttributeName,$ruleAttributeValue);
        }
        return $ruleDataModel;
    }

   protected function _storeFailedTransaction($_exception,$row, $_record){

            $_failedDiscountImportModel = $this->_failedDiscountImportFactory->create();
            $this->_logger->critical("We got an error on processing row $row anything else here to do?");
            $this->_logger->critical($_exception->getMessage());


            $_jsonLogMessage = [
                'message'=>$_exception->getMessage(),
                'transactionRow'=>$row,
                'transactionData'=>$_record
            ];
            $_jsonLogMessage = json_encode($_jsonLogMessage);

            $_failedDiscountImportModel
                ->setData('log_message',$_jsonLogMessage)
                ->save();

        }

}

