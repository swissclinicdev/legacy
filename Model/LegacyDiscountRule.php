<?php

namespace Swissclinic\Legacy\Model;

class LegacyDiscountRule  extends \Magento\Framework\Model\AbstractModel  {

    protected function _construct() {
        $this->_init(\Swissclinic\Legacy\Model\ResourceModel\LegacyDiscountRule::class);

    }

}