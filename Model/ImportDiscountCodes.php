<?php

namespace Swissclinic\Legacy\Model;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Model\Coupon;


class ImportDiscountCodes {


    protected $_logger;
    protected $_couponRepositoryInterfaceFactory;
    protected $_couponDataInterfaceFactory;
    protected $_ruleAttributeMapping = array();
    protected $_excludedColumns = array();
    protected $_couponAttributeHandlersFactory;


    /**
     * ImportDiscountCodes constructor.
     * @param LegacyLoggerFactory $logger
     * @param \Magento\SalesRule\Model\CouponRepositoryFactory $_couponRepositoryInterfaceFactory
     * @param \Magento\SalesRule\Api\Data\CouponInterfaceFactory $_couponDataInterfaceFactory
     * @param CouponAttributeHandlersFactory $_couponAttributeHandlersFactory
     */
    public function __construct(
        LegacyLoggerFactory $logger,
        \Magento\SalesRule\Model\CouponRepositoryFactory $_couponRepositoryInterfaceFactory,
        \Magento\SalesRule\Api\Data\CouponInterfaceFactory $_couponDataInterfaceFactory,
        CouponAttributeHandlersFactory $_couponAttributeHandlersFactory

    ){
        $this->_logger = $logger;
        $this->_couponRepositoryInterfaceFactory = $_couponRepositoryInterfaceFactory;
        $this->_couponDataInterfaceFactory = $_couponDataInterfaceFactory;
        $this->_couponAttributeHandlersFactory = $_couponAttributeHandlersFactory;
        $this->_excludedColumns = array(
            'autoinlosen'=>true, // Askås "Aktivera automatisk inlösen i Kassan"
            'autoinlosen_begransasprak'=>true,
            'forbrukad'=>true,
            'begransasprak'=>true, // Will be used later on calling the handler for isocountry.
            'beskr_en'=>true, // store_labels will be handled by the name handler and store label handler.
            'beskr_fi'=>true, // store_labels
            'beskr_no'=>true, // store_labels
            'beskr_sv'=>true, // store_labels
            'beskr_dk'=>true, // store_labels
            'beskr_es'=>true, // store_labels
            'beskr_nl'=>true, // store_labels
            'engangskod'=>true,
            'georderrabatt_grans'=>true, // will be handled by condition.
            'georderrabatt_grans_artiklar'=>true, // will be handled by condition.
            'begransa_antal'=>  true,
                                    /*
                                       N (Obegränsat antal i varukorgen)|
                                       Y (Begränsa till) | If this one is active field "rabattkod_Begransa_Antal_Till" will be used.
                                       A (Begränsa till artikel nr ) If this one is active fields "rabattkod_Begransa_Antal_Start" and "rabattkod_Begransa_Antal_Stopp"
                                    */
            // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.

            // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal_start'=>true, // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal_stopp'=>true, // FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'begransa_antal_till'=>true, // Depends on begransa_antal = Y FROM THE INFORMATION RECEIVED, this or not used that much, so records that have that this field with data, will be moved to manual check.
            'beskr_intern'=>'name',
            'extra_datumbegransning_start'=>'from_date', // fromDate 1381356000
            'extra_datumbegransning_stopp'=>'to_date', // toDate 1381356000
            //'forbrukad' =>true, // 0 Not used in the parents found
            'fraktfritt' =>'simple_free_shipping', // SimpleFreeShipping Value in Askås are: Y|N
            'georderrabatt' =>'condition', // In Askås is Skall kampanjen berättiga till en total orderrabatt? With option N | Y


            /*

            In Askås this is named, Begränsa erbjudandet till en gång per kund?
            With option to:

            Y (Ja, en gång per kundnr) |
            Y2 (Ja, en gång per kundnr och/eller ip-adress)|
            N (Nej)

            */

            'georderrabatt_begransa_artikelgrupp' =>true,
            /*

            In Askås is named, Begränsa vilka artiklar som ska ingå i totalsumma?
            This one is of type string, on the frontend is presented as a multiple choose input,
            containing integers that link to work group, such as;
            190 for the article group, 219 for beauty treatments or 201 Dieter... and so on.

            Is dependent of "GeOrderRabatt_Begransa_Artikelgrupp_Typ"  with options 1 (Inkludera) 2 (Exkludera)
            Depending of the options the selected ids stored in georderrabatt_begransa_artikelgrupp will be exluded or included.
            Here we have the first link with the table: swissclinic_rabattkoder_begransningar. Which connects the id of the rule or campaign with with "Typ".

            Rabatt_ID,"Field","Data","Typ"
            624014,"GeOrderRabatt_Begransa_Artikelgrupp","'191'","Allow".

            */
            'georderrabatt_procent' =>'action',
            /* In Askås, is named Totalrabatt i procent. Of type float, 0.00 -
            However needs to have GeOrderRabatt_Typ set to 2 (Totalrabatt i procent). As it looks this column is not available for export
             from the source so in this case we'll make sure that if the field is the we should use this information.
            For cases in which both georderrabatt_procent and georderrabatt_summa are set we should require manual check.
             */

            'georderrabatt_summa'=>'action',
            /*
             In Askås is named Totalrabatt, SEK inkl. moms, with values of type float, for example "360.00".
             As it looks this column is not available for export
             from the source so in this case we'll make sure that if the field is the we should use this information. For cases in which both georderrabatt_procent and
             georderrabatt_summa are set we should require manual check
            */

            'tid'=>true, // This one is used not available via Magento\SalesRule\Api\Data\RuleInterface - however it should be saved in a separate table for future use + record keeping. legacy_tid
            'tid_uppdaterad'=>true, // This one is used not available via Magento\SalesRule\Api\Data\RuleInterface - however it should be saved in a separate table for future use + record keeping. legacy_tid_uppdaterad
            'isocountry'=>'website_ids' // Will be used for handling the different websites in combination with begransasprak, isoCountry will have higher priority and will set the countries were the rule will apply.

        );


        $this->_ruleAttributeMapping = [
            'rabatt_foralder'=>'rule_id',
            'rabattkod'=>'code', // This one should only is only used on coupon codes. However on parents with child it should be stored in legacy_rabattkod
            'georderrabatt_begransakund' =>'usage_per_customer'
        ];
    }

    /**
     * @param null $filename
     */
    public function import($filename=null){
        if($filename==null){
            throw \Exception('type the name of the CSV file containing the data.');
        }
        if(!file_exists($filename )){
            echo "Sorry, cannot open the file! \n";
            exit();
        }
        $this->_import($filename);
    }

    /**
     * @param $filename
     */
    protected function _import($filename){
        $_logger = $this->_logger->create();
        $row =0;
        try{

            ini_set('auto_detect_line_endings',TRUE);

            if (($handle = fopen($filename , "r")) !== FALSE) {

                $columnNames = array();
                $errors= array();

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    // Ok so it is the first row? get the column names!
                    if(empty($columnNames)){
                        $columnNames = array_merge($columnNames,$data);
                        continue;
                    }

                    $_columnNameDataRow = array_combine($columnNames,$data);
                    $couponDataModel = $this->_getCouponDataModel($_columnNameDataRow);

                    try{

                        if(!$couponDataModel){
                            $errors[]= $row;
                            $_message = new \Magento\Framework\Phrase("Error occurred when saving coupon: ". json_encode($data) ) ;
                            throw new \Magento\Framework\Exception\LocalizedException(
                                $_message
                            );
                            continue;
                        }

                        if(!$couponDataModel->getRuleId()){
                            throw \Magento\Framework\Exception\NoSuchEntityException::singleField('rule_id', $couponDataModel->getRuleId());
                        }

                        $_couponRepositoryModel = $this->_couponRepositoryInterfaceFactory->create();
                        $_couponRepositoryModel->save($couponDataModel);
                    }
                    catch (\Magento\Framework\Exception\NoSuchEntityException $NoSuchEntityException){
                        $errors[]= $row;
                        $_logger->error($NoSuchEntityException->getMessage());
                        continue;
                    }
                    catch(\Magento\Framework\Validator\Exception $avalidatorException){
                        $_logger->error($avalidatorException->getMessage());
                        $errors[]= $row;
                        continue;
                    }
                    catch (\Magento\Framework\Exception\LocalizedException $localizedException){
                        $_logger->error($localizedException->getMessage());

                        $errors[]= $row;
                        continue;

                    }

                    $row++;
                }
                echo "Total number of processed discount codes: $row \n";
                $_errorCount = count($errors);

                if($_errorCount>0){
                    echo "Total of errors found: $_errorCount \n";
                }

                fclose($handle);
            }
        }
        catch(\Exception $e){
            $this->_logger->critical("We got an error on processing row $row anything else here to do?");
            $this->_logger->critical($e->getMessage());
            exit();
        }
    }

    /**
     * @param $_dataRow
     * @return mixed
     */
    protected function _getCouponDataModel($_dataRow){

        $_arrRuleData = $this->_getRuleDataRow($_dataRow);
        if(!$_arrRuleData){
            return false;
        }

        $_couponDataModel = $this->_couponDataInterfaceFactory->create();
        foreach ($_arrRuleData as $ruleAttributeName => $ruleAttributeValue){
            $_couponDataModel->setData($ruleAttributeName,$ruleAttributeValue);
        }
        return $_couponDataModel;
    }


    /**
     * @param array $dataRow
     * @return array
     */
    protected function _getRuleDataRow(array $dataRow){

        $_attributesRequiringManualCheck = [
            'georderrabatt_begransa_artikelgrupp'=>true
        ];
        $_arrCouponData = [

            Coupon::KEY_RULE_ID => '',
            Coupon::KEY_CODE => '',
            Coupon::KEY_USAGE_LIMIT => false,
            Coupon::KEY_USAGE_PER_CUSTOMER => '',
            Coupon::KEY_USAGE_PER_CUSTOMER => '',
            Coupon::KEY_CREATED_AT=>'',
            Coupon::KEY_TYPE =>''
        ];

        // Going forward for each column.

        foreach ($dataRow as $_columnName => $_value){

            $_attributeHandlersModel = $this->_couponAttributeHandlersFactory->create();
            if( !isset($this->_excludedColumns[$_columnName])){

                /*
                 * Let's update the rule data object with the data retrieved from the rows found in the csv file.
                 */

                if(isset($this->_ruleAttributeMapping[$_columnName]) &&
                    ($magentoAttributeName = $this->_ruleAttributeMapping[$_columnName])){

                    // Here we catch all attributes that if set
                    if(isset($_attributesRequiringManualCheck[$_columnName]) && $_value ){
                        continue;
                    }

                    if(isset($_arrCouponData[$magentoAttributeName])){

                        // Ok, so if the method exists it can be later on be called and will convert the data retrieved into something that can be used by Magento.
                        if(
                            method_exists($_attributeHandlersModel, $magentoAttributeName) &&
                            $_arrCouponData = $_attributeHandlersModel->$magentoAttributeName(
                                $_arrCouponData, $dataRow
                            )
                        ){

                            if(!$_arrCouponData){
                                continue;
                            }
                        }
                        else {
                                continue;
                        }
                    }
                }
            }
        }
        return $_arrCouponData;
    }

}

