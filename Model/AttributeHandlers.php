<?php

namespace Swissclinic\Legacy\Model;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Model\Data\Rule;

class AttributeHandlers {

    const BESKR = 'beskr_';
    const MIN_FLOAT_VALUE = 0.00;
    const GREATER_THAN = '>';
    const CUSTOMER_GROUP_GENERAL= '1'; // General

                    /*
                     * setName()
                     * setStoreLabels()
                     * setDescription
                     * setWebsiteIds
                     * setCustomerGroupIds
                     * setFromDate
                     * setToDate
                     * setUsesPerCustomer
                     * setIsActive
                     * setCondition (ConditionInterface)
                     * setActionCondition (ConditionInterface)
                     * setStopRulesProcessing
                     * setIsAdvanced
                     * setProductIds
                     * setSortOrder
                     * setSimpleAction
                     * setDiscountAmount
                     * setDiscountQty
                     * setDiscountStep
                     * setSimpleFreeShipping
                     * setUsesPerCoupon
                     * setUseAutoGeneration
                     * setCouponType
                     * setIsRss
                     * setTimesUsed
                     * setApplyToShippings
                     */


    protected $columnNames;
    protected $_logger;
    protected $_landListGroupsFromSource;
    protected $_websiteId;
    protected $_websiteCollection;
    protected $_conditionDataInterfaceFactory;
    protected $_ruleConditionAddressFactory;
    protected $_websiteFactory;
    protected $_ruleFactory;
    protected $_ruleLabelDataInterfaceFactory;



    /**
     * AttributeHandlers constructor.
     * @param \Magento\SalesRule\Api\Data\RuleLabelInterfaceFactory $ruleLabelDataInterfaceFactory
     * @param \Magento\SalesRule\Api\Data\ConditionInterfaceFactory $conditionDataInterfaceFactory
     * @param \Magento\SalesRule\Model\Rule\Condition\AddressFactory $ruleConditionAddressFactory
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\SalesRule\Api\Data\RuleLabelInterfaceFactory $ruleLabelDataInterfaceFactory,
        \Magento\SalesRule\Api\Data\ConditionInterfaceFactory $conditionDataInterfaceFactory,
        \Magento\SalesRule\Model\Rule\Condition\AddressFactory $ruleConditionAddressFactory,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_logger = $logger;
        $this->_ruleLabelDataInterfaceFactory = $ruleLabelDataInterfaceFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->_conditionDataInterfaceFactory = $conditionDataInterfaceFactory;
        $this->_ruleConditionAddressFactory = $ruleConditionAddressFactory;

        // Mapping of code of source server where the rule was created and the countries where should be applied.
        $this->_landListGroupsFromSource = [
            'SV'=> array('se','no','fi','gb'),
            'DK'=> array('dk'),
            'ES'=> array('es'),
            'NL'=> array('nl','be'),
            'BR'=> array('br'),
            'BASE'=> array('base') // For tests
        ];

        // Mapping of iso code from source with website code in Magento.
        $this->_websiteId= array(
            'se' =>'sweden',
            'gb' =>'greatbritain',
            'fi' =>'finland',
            'be' =>'belgium',
            'dk' =>'denmark',
            "no" =>'norway',
            "nl" =>'netherlands',
            "es" =>'spain',
            'base' =>'base' // For tests
        );
    }



    /**
     * @param array $arrData
     * @param $strValue
     * @return array|bool
     */
    public function simple_free_shipping(array $arrData, $dataRow){
        $strValue = (string) $dataRow['fraktfritt'];
        if(!$strValue){
            $strValue= 'N';
        }
        $_option = array('Y' => 1, 'N' => 0);
        $return = $arrData;

        if(!isset($_option[$strValue]) ){
            // Ok, so the option is not here!!! Abort!!
            return false;
        }

        $return['simple_free_shipping'] = $_option[$strValue];
        return  $return;
    }


    /**
     * @param array $arrData
     * @param $dataRow
     * @return array
     */
    public function from_date(array $arrData, $dataRow){
        // We need to convert the epoc date to YYYY-MM-DD
        $convertedDate = $this->epochConverter($dataRow['extra_datumbegransning_start']);
        $arrData['from_date'] = $convertedDate;
        return  $arrData;
    }

    /**
     * @param array $arrData
     * @param $dataRow
     * @return array
     */
    public function to_date(array $arrData, $dataRow){
        // We need to convert the epoc date to YYYY-MM-DD
        $convertedDate = $this->epochConverter($dataRow['extra_datumbegransning_stopp']);
        $arrData['to_date'] = $convertedDate;
        return  $arrData;
    }

    /**
     * @param $epoch
     * @return mixed
     */
    private function epochConverter($epoch){
        if(!$epoch){
            return $epoch;
        }
        $dataTime = new \DateTime("@$epoch");
        $convertedDate = $dataTime->format('Y-m-d');
        return $convertedDate;
    }

    /**
     * The idea of the method is to set the storeLabels of the rule.
     *
     *
     */
    public function store_labels($fieldName, $strLabel){

        $_end = strlen(self::BESKR);
        $_countryId = substr($fieldName,$_end);
        if(!isset($this->_websiteId[$_countryId])){
            // No website code? Abort!
            return false;
        }
        $_ruleLabelDataModel = $this->_ruleLabelDataInterfaceFactory->create();
        $_websiteModel = $this->_websiteFactory->create();

        $_strWebsiteCode = $this->_websiteId[$_countryId];
        $_websiteModel->load($_strWebsiteCode);

        $_storeId = $_websiteModel->getDefaultStoreId();
        $_ruleLabelDataModel->setStoreId($_storeId)->setStoreLabel($strLabel);

        return $_ruleLabelDataModel;
    }

    /**
     * @param $begransasprak
     * @param $isocountry
     * @return array|bool
     *
     * The idea of this method is to translate the two fields located in Askås, "begransasprak" and "isocountry"
     * to a list of Magento website codes where the rule should apply. As it looks isoCountry refers to the server where the rules was created,
     * this server can have several countries inside, such as UK,NL... and so on.
     *
     */

    public function website_ids($_arrRuleData, $dataRow){

        $begransasprak=$dataRow['begransasprak'];
        $isocountry=$dataRow['isocountry'];
        $return = array();

        if(!$isocountry){
            // Ok, so we got record that does not contain any country! This type of rule should be stored for manual check.
            return false;
        }
        $isocountry = strtoupper($isocountry);
        $_arrBegransasprak = explode(',',strtolower(str_replace("'","",$begransasprak)));
        $_finalListOfCountries = (isset($this->_landListGroupsFromSource[$isocountry]))? $this->_landListGroupsFromSource[$isocountry] :array();


        /*
         * Right, now that we got a list of countries which are under the parent server. (parent server == isocountry)
         * Is is time to check which countries are not eligible of this rule.
         */

        if(!empty($_arrBegransasprak)){
            $_finalListOfCountries = array_diff($_finalListOfCountries,$_arrBegransasprak);
        }

        // make sure that any country is returned if not then something is wrong here! Manual check needed as well.
        if(empty($_finalListOfCountries)) {
            return false;
        }

        // Now we got the final list of countries were the rule should be applied.

        foreach($_finalListOfCountries as $countryCode){

            if(($_websiteCode = $this->_getWebsiteCode($countryCode))){
                if(($_website_id = $this->_getWebsiteId($_websiteCode))){
                    $return[]= $_website_id;
                }

            }
        }

        $_arrRuleData[Rule::KEY_WEBSITES]= $return;
        return $_arrRuleData;
    }


    private function _getWebsiteId($_websiteCode=false){

        if(!$_websiteCode){
            return $_websiteCode=false;
        }

        $_return = false;

        $_websiteModel = $this->_websiteFactory->create();
        $_websiteModel->load($_websiteCode);

        if(($_website_id = (int) $_websiteModel->getId())){
            $_return = $_website_id;
        }

        return $_return;
    }


    /**
     * @param $_arrRuleData
     * @param array $_condition
     * @return mixed
     */
    public function condition ($_arrRuleData, array $_condition){
        $_return = false;

        $conditionDataModel = $this->_conditionDataInterfaceFactory->create();
        // Get the conditions.
        //
        $conditionDataModel->setData('aggregator_type','all')
            ->setData('operator',null)
            ->setData('condition_type','Magento\SalesRule\Model\Rule\Condition\Combine');

        // This means that in the source rule the cart is set to be above a specific total amount.

        $_ruleConditionAddressModel = $this->_getListOfConditions($_condition);
        $conditionDataModel->setData('conditions', $_ruleConditionAddressModel);
        $_arrRuleData['condition'] = $conditionDataModel;

        // Process the actions to be applied.
        $_return = $this->action($_arrRuleData,$_condition);

        return $_return;

    }


    /**
     * @param $_arrRuleData
     * @param array $dataRow
     * @return mixed
     */
    public function action ($_arrRuleData, array $dataRow){

        $_simpleAction = false ;
        $_discountAmount = false ;

        $georderrabatt_procent = floatval($dataRow['georderrabatt_procent']);
        $georderrabatt_summa = floatval($dataRow['georderrabatt_summa']);

        if($georderrabatt_procent > self::MIN_FLOAT_VALUE && $_discountAmount = $georderrabatt_procent){
            $_simpleAction = RuleInterface::DISCOUNT_ACTION_BY_PERCENT;
        }
        elseif( $georderrabatt_summa > self::MIN_FLOAT_VALUE && $_discountAmount = $georderrabatt_summa) {
            $_simpleAction = RuleInterface::DISCOUNT_ACTION_FIXED_AMOUNT;
        }

        $_arrRuleData[Rule::KEY_SIMPLE_ACTION] = $_simpleAction;
        $_arrRuleData[Rule::KEY_DISCOUNT_AMOUNT] = $_discountAmount;


        return $_arrRuleData;
    }


    /**
     * @param $_condition
     * @return mixed
     */
    private function _getListOfConditions($_condition){


        $_ruleConditionAddressModel = array();
        $_georderrabatt = (isset($_condition['georderrabatt'])
            && $_condition['georderrabatt']=='Y')?
            $_condition['georderrabatt']:false; // Y N

        if(!$_georderrabatt){
            return $_ruleConditionAddressModel;
        }

        if( isset($_condition['georderrabatt_grans']) && ($_value = floatval($_condition['georderrabatt_grans'])) && $_value > self::MIN_FLOAT_VALUE
            && ($_attributeCode = 'base_subtotal') && ($_operator = self::GREATER_THAN)

        ){
            // TODO: We need to confirm if the total ammount from Askås are with taxes or not.
            // Interesting Askås, says Vid köp över, SEK inkl. moms, this means that it has to be >
            $_ruleConditionAddressModel[] = $this->_getRuleConditionAddressModel($_attributeCode,$_operator,$_value);
        }

        if( isset($_condition['georderrabatt_grans_artiklar'])
            && ($_value = $_condition['georderrabatt_grans_artiklar']) && ($_attributeCode = 'total_qty') && ($_operator = self::GREATER_THAN)
        ) {
            $_ruleConditionAddressModel[] = $this->_getRuleConditionAddressModel($_attributeCode,$_operator,$_value);
        }
        return $_ruleConditionAddressModel;

    }

    /**
     * @return string
     */
    public function customer_group_ids($_arrRuleData,$value){
        $_arrRuleData['customer_group_ids']= self::CUSTOMER_GROUP_GENERAL;
        return $_arrRuleData;
    }

    /**
     * @param $_countryIsoCode
     * @return bool|mixed
     */
    private function _getWebsiteCode($_countryIsoCode){
        $return = false;
        if(isset($this->_websiteId[$_countryIsoCode])){
            $return =  $this->_websiteId[$_countryIsoCode];
        }
        return $return;
    }

    /**
     * @param $attributeName
     * @param $operator
     * @param $value
     * @return mixed
     */
    private function _getRuleConditionAddressModel($attributeName, $operator, $value){

        $_ruleConditionAddressModel = $this->_conditionDataInterfaceFactory->create();
        $_ruleConditionAddressModel
            ->setData('attribute_name',$attributeName)
            ->setData('operator',$operator)
            ->setData('condition_type','Magento\SalesRule\Model\Rule\Condition\Address')
            ->setData('value',$value);

        return $_ruleConditionAddressModel;

    }

    /**
     * @param $_arrRuleData
     * @param array $dataRow
     * @return mixed
     */
    public function name($_arrRuleData, array $dataRow){

        $_arrRuleData['name'] = $dataRow['beskr_intern'];
        // Take care of the labels.

        $listLabels = array(
            'beskr_en'=> $dataRow['beskr_en'],
            'beskr_fi'=> $dataRow['beskr_fi'],
            'beskr_no'=> $dataRow['beskr_no'],
            'beskr_sv'=> $dataRow['beskr_sv'],
            'beskr_dk'=> $dataRow['beskr_dk'],
            'beskr_es'=> $dataRow['beskr_es'],
            'beskr_nl'=> $dataRow['beskr_nl']
        );

        foreach ($listLabels as $fieldName => $strLabel){
            if(($store_label = $this->store_labels($fieldName,$strLabel))){
                $_arrRuleData['store_labels'][] = $store_label;
            }
        }

        return $_arrRuleData;
    }

}

